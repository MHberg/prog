#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <math.h>

int func (double t, const double y[], double dydx[], void *params) {
	(void)(t);
	double eps = *(double *)params;
	dydx[0] = y[1];
	dydx[1] = 1-y[0]+eps*y[0]*y[0]; 
	return GSL_SUCCESS;
}

int main() {
	double eps[3] = {0.0, 0.0, 0.01};
	char dataIndex[3][40] = {"# First data block (index 0)\n", "\n\n# Second data block (index 1)\n", "\n\n# Third data block (index 2)\n"};
	double u0_i = 1.0;
	double u1_i[3] = {0.0, -0.5, -0.5};
	int N = 5000;
	double phi_final = 196*M_PI;
	gsl_odeiv2_system sys;
	sys.function = func;
	sys.jacobian = NULL;
	sys.dimension = 2;
	for (int j=0; j<3; j++) {
		double epsj = eps[j];
		sys.params = &epsj;
		gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rkf45, 1e-6, 1e-6, 1e-6);
		printf("%s",dataIndex[j]);
		int i;
		double phi = 0.0; 
		double u[2] = {u0_i, u1_i[j]};
		for (i = 0; i <= N; i++) {
			double phi_i = i * phi_final / (N/1.0);
			int status = gsl_odeiv2_driver_apply (d, &phi, phi_i, u);
			if (status != GSL_SUCCESS) {
				printf ("error, return value=%d\n", status);
				break;
			}
			printf ("%.5e %.5e\n", phi, u[0]);
		}
		gsl_odeiv2_driver_free (d);
	}
	return 0;
}
