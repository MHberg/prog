typedef struct {int n; double *x, *y, *b, *c;} qspline;

qspline* qspline_alloc(int n, double *x, double *y); 

double qspline_eval(qspline *s, double z);

void qspline_free(qspline *s);

double qspline_derivative(qspline *s, double z);

double qspline_integral(qspline *s, double z);

double linterp_integ(int n, double *x, double *y, double z);

double linterp(int n, double *x, double *y, double z);

#define f(x) sin(x)
