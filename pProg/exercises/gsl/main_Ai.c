#include<stdio.h>
#include "gsl/gsl_sf_airy.h"

int main() {
	double x;
	for (int i=-1500; i<100; i++){
		x = i/100.0;
		printf("%lg \t %lg\n",x,gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE));
	}
	return 0;
}
