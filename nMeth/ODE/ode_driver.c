#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ode.h"
#define ode_stepper rkstep12

int ode_driver(
	gsl_vector* xlist,
	gsl_matrix* ylist,
	double b,
	double h,
	double acc,
	double eps,
	int max,
	void (*f)(double x, gsl_vector* y, gsl_vector* dydx))
{
	int n = ylist->size2;
	int i, k=0;
	double x, s, err, normy, tol, a = gsl_vector_get(xlist,0);
	gsl_vector* yh = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
       	while(gsl_vector_get(xlist,k)<b) {
		x = gsl_vector_get(xlist,k), gsl_matrix_get_row(y,ylist,k);
		if(x+h>b) h = b-x;
		ode_stepper(x,h,y,f,yh,dy);
		s = 0;
		for (i=0; i<n; i++) {
			s += pow(gsl_vector_get(dy,i),2);
			err = sqrt(s);
		}
		s = 0;
		for (i=0; i<n; i++) {
			s += pow(gsl_vector_get(yh,i),2);
			normy = sqrt(s);
		}
		tol = (normy*eps+acc)*sqrt(h/(b-a));
		if (err<tol) { // step is accepted and function continues
			k++; if(k>max-1) return -k;
			gsl_vector_set(xlist,k,x+h);
			for (i=0; i<n; i++) gsl_matrix_set(ylist,k,i,gsl_vector_get(yh,i));
		}
		if(err>0) h *= pow(tol/err,0.25)*0.95;
		else h *= 2;
	} // end while loop
	return k+1;
	gsl_vector_free(yh);
	gsl_vector_free(dy);
	gsl_vector_free(y);
}
