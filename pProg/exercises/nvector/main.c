#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#include<math.h>

#define RND (double)rand()/RAND_MAX
int double_equal(double x, double y, double epsilon) {
	if (fabs(x-y)<= epsilon) return 1;
	else return 0;
}

int main()
{
	int n = 5;

	double epsilon = 0.000001;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (double_equal(vi, value,epsilon)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_dot_product ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	double result = 0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		result += x*y;
	}
	double actualResult = nvector_dot_product(a,b);
	printf("\na dot b should   = %lg\n", result);
	printf("\na dot b actually = %lg\n", actualResult);

	if (double_equal(result,actualResult,epsilon)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_add ...\n");
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	return 0;
}
