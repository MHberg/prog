void newton(
	void (*f)(gsl_vector* x, gsl_vector* fx), 
	gsl_vector* x, 
	double dx,
	double eps
	);

void newton_with_jacobian(
	void (*f)(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), 
	gsl_vector* x, 
	double dx,
	double eps);
