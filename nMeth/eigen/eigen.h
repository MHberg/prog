int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

int jacobi_eigbyeig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int n);
