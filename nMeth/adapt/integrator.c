#include<math.h>
#include<assert.h>
#include<stdio.h>

double adapt24(double f(double), double a, double b, double acc, double eps, double f2, double f3, int nrec, double* err) {
	assert(nrec<1000000);
	double f1 = f(a+(b-a)/6), f4 = f(a+5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4)/6*(b-a), q = (f1+f2+f3+f4)/4*(b-a);
	double tol = acc+eps*fabs(Q); 
	*err = fabs(Q-q)/2;
	if(*err < tol) return Q;
	else {
		double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2.0),eps,f1,f2,nrec+1,err);
		double Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2.0),eps,f3,f4,nrec+1,err);
		return Q1+Q2;
	}
}

double adapt(double f(double), double a, double b, double acc, double eps, double* err) {
	double f2, f3;
	int nrec = 0;
	if (isinf(a) && isinf(b)) {
		double g(double t) {
			if (a < 0 && b > 0) return f(t/(1-t*t))*(1+t*t)/((1-t*t)*(1-t*t));
			else if (a > 0 && b < 0) return -f(t/(1-t*t))*(1+t*t)/((1-t*t)*(1-t*t));
			else return 0;
		}
		double a_new = -1;
		double b_new = 1;
		f2 = g(a_new+2*(b_new-a_new)/6), f3 = g(a_new+4*(b_new-a_new)/6); 
		return adapt24(g,a_new,b_new,acc,eps,f2,f3,nrec,err);
	} else if (isinf(a) && a < 0) {
		double g(double t) { return f(b+t/(1+t))*1/((1+t)*(1+t));}
		double a_new = -1, b_new = 0;
		f2 = g(a_new+2*(b_new-a_new)/6); f3 = g(a_new+4*(b_new-a_new)/6); 
		return adapt24(g,a_new,b_new,acc,eps,f2,f3,nrec,err);
	} else if (isinf(a) && a > 0) {
		double g(double t) { return -f(b+t/(1-t))*1/((1-t)*(1-t));}
		double a_new = 0, b_new = 1;
		f2 = g(a_new+2*(b_new-a_new)/6); f3 = g(a_new+4*(b_new-a_new)/6); 
		return adapt24(g,a_new,b_new,acc,eps,f2,f3,nrec,err);
	} else if (isinf(b) && b > 0) {
		double g(double t) { return f(a+t/(1-t))*1/((1-t)*(1-t));}
		double a_new = 0, b_new = 1;
		f2 = g(a_new+2*(b_new-a_new)/6); f3 = g(a_new+4*(b_new-a_new)/6); 
		return adapt24(g,a_new,b_new,acc,eps,f2,f3,nrec,err);
	} else if (isinf(b) && b < 0) {
		double g(double t) { return -f(a+t/(1+t))*1/((1+t)*(1+t));}
		double a_new = -1, b_new = 0;
		f2 = g(a_new+2*(b_new-a_new)/6); f3 = g(a_new+4*(b_new-a_new)/6); 
		return adapt24(g,a_new,b_new,acc,eps,f2,f3,nrec,err);
	} else {	
		double f2 = f(a+2*(b-a)/6); f3 = f(a+4*(b-a)/6); int nrec = 0;
		return adapt24(f,a,b,acc,eps,f2,f3,nrec,err);
	}
}

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps, double* err) { 
	double g(double t) {
		return f( (a+b)/2+(b-a)/2*cos(t) )*sin(t)*(b-a)/2;
	}
	return adapt(g,0,M_PI,acc,eps,err);
}
