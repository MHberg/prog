Newton minimization: 
__________________________

eps = 1.0e-03

Minimization of Rosenbrock's valley function:
Start point: 
-3
5
Function value (start): 1616
Steps = 11621
Minimum of function found at: 
1
0.999997
Function value (min): 7.65855e-10
Gradient at min:
0.000553146
-0.000553472

Minimization of Himmelblau's function:
Start point: 
-3
5
Function value (start): 234
Steps = 33
Minimum of function found at: 
-2.80513
3.13131
Function value (min): 7.02904e-09
Gradient at min:
-0.000955394
4.9811e-11

Quasi-Newton minimization with Broyden's update:
________________________ 

eps = 1.0e-03

Minimization of Rosenbrock's valley function:
Start point: 
-3
5
Function value (start): 1616
Steps = 51
Minimum of function found at: 
1.00003
1.00007
Function value (min): 1.33804e-09

Minimization of Himmelblau's function:
Start point: 
-3
5
Function value (start): 234
Steps = 46
Minimum of function found at: 
-2.80511
3.13131
Function value (min): 2.85741e-09
eps = 1.0e-03

Non-linear least-squares fitting problem:
Start values: 
A =	5
T =	1
B =	0
Function value (start): 395.83
Steps = 43
Minimum of function found at: 
A =	3.55703
T =	3.20545
B =	1.23192
Function value (min): 2.06796


Downhill simplex minimization:
______________________________ 

Simplex size goal = 1e-06

Minimization of Rosenbrock's valley function:
Exact minimum: f(1, 1)=0
Initial simplex:
 11 5
 5 11
 5 5
Final simplex:
 1 0.999999 ; function value: 1.20661e-13
 1 1 ; function value: 1.80548e-13
 1 1 ; function value: 3.1608e-13


Minimization of Himmelblau's function:
Exact minimum 1: f(3, 2)=0
Exact minimum 2: f(-2.805, 3.131)=0
Exact minimum 3: f(-3.779, -3.283)=0
Exact minimum 4: f(3.584, -1.848)=0
Initial simplex:
 11 5
 5 11
 5 5
Final simplex:
 3 2 ; function value: 1.07463e-12
 3 2 ; function value: 3.65113e-12
 3 2 ; function value: 1.29596e-12


Non-linear least-squares fitting problem:
Initial simplex:
 11 5 5
 5 11 5
 5 5 11
 5 5 5
Final simplex:
 3.55702 3.20543 1.23193 ; function value: 2.06796
 3.55702 3.20543 1.23193 ; function value: 2.06796
 3.55702 3.20542 1.23193 ; function value: 2.06796
 3.55702 3.20543 1.23193 ; function value: 2.06796
