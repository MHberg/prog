#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"newton.h"
#define FMT "%10.4g"

void rootFindNumJac() {
	// starting condition for all systems
	double x_init[2] = {2.0, -3.0};

	printf(" Solving system of linear equations: \n"); 
	// A1: Solve system of equations
	double A = 10000;
	int ncalls = 0;
	void eqsyst(gsl_vector* p, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, A*x*y-1);
		gsl_vector_set(fx,1, exp(-x)+exp(-y)-1-1.0/A);
	}
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector_set(x,0,x_init[0]);
	gsl_vector_set(x,1,x_init[1]);
	gsl_vector* fx = gsl_vector_alloc(2);
	printf("  Initial vector x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	eqsyst(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);
	double dx = 1e-9;
	double eps = 1e-6;
	newton(eqsyst,x,dx,eps);
	fprintf(stderr,"\t1\n");
	printf("\n  Number of calls to f: %i\n\n",ncalls);
	fprintf(stderr,"%i\t1\n",ncalls);
	printf("  Solution x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	eqsyst(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);

	gsl_vector_free(x);
	gsl_vector_free(fx);
	printf("\n\n");

	ncalls = 0;
	// Find minimum of Rosenbrock
	void rosenf(gsl_vector* p, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+200*(y-x*x)*(-2*x));
		gsl_vector_set(fx,1, 200*(y-x*x));
	}
	x = gsl_vector_alloc(2);
	gsl_vector_set(x,0,x_init[0]);
	gsl_vector_set(x,1,x_init[1]);
	fx = gsl_vector_alloc(2);
	printf(" Extremum of Rosenbrock's valley function:\n");
	printf("  Initial vector x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	rosenf(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);
	newton(rosenf,x,dx,eps);
	fprintf(stderr,"\t2\n");
	printf("\n  Number of calls to f: %i\n\n",ncalls);
	fprintf(stderr,"%i\t2\n",ncalls);
	printf("  Solution x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	rosenf(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);

	gsl_vector_free(x);
	gsl_vector_free(fx);
	printf("\n\n");

	ncalls = 0;
	// Find minimum of Himmelblau's function
	void himmelf(gsl_vector* p, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));
		gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
	}
	x = gsl_vector_alloc(2);
	gsl_vector_set(x,0,x_init[0]);
	gsl_vector_set(x,1,x_init[1]);
	fx = gsl_vector_alloc(2);
	printf(" Extremum of Himmelblau's function:\n");
	printf("  Initial vector x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	himmelf(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);
	newton(himmelf,x,dx,eps);
	fprintf(stderr,"\t3\n");
	printf("\n  Number of calls to f: %i\n\n",ncalls);
	fprintf(stderr,"%i\t3\n",ncalls);
	printf("  Solution x: \n");
	gsl_vector_fprintf(stdout,x,FMT);
	himmelf(x,fx);
	printf("  f(x): \n");
	gsl_vector_fprintf(stdout,fx,FMT);

	gsl_vector_free(x);
	gsl_vector_free(fx);
	printf("\n\n");
}
