void simplex_update(
		double** simplex, double* f_values, int d,
		int* hi, int* lo, double* centroid) {

	*hi = 0; 
	double highest = f_values[0];
	*lo = 0; 
	double lowest = f_values[0];

	for (int k=1; k<d+1; k++) {
		double next = f_values[k];
		if (next>highest) {highest = next; *hi = k;}
		if (next<lowest) {lowest = next; *lo = k;}
	}

	for (int i=0; i<d; i++) {
		double sum = 0;
		for (int k=0; k<d+1; k++) if (k!=*hi) sum += simplex[k][i];
		centroid[i] = sum/d;
	}
}

void simplex_initiate(
			double (*fun)(double*),
			double** simplex, double* f_values, int d,
			int* hi, int* lo, double* centroid) {

	for (int k=0; k<d+1; k++) f_values[k] = fun(simplex[k]);

	simplex_update(simplex,f_values,d,hi,lo,centroid);
}
