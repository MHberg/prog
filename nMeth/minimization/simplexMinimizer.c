#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "simplex.h"

double rosenf3(double* point) {
	double x=point[0];
	double y=point[1];
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmelf3(double* point) {
	double x=point[0];
	double y=point[1];
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

double t3[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y3[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e3[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N3 = sizeof(t3)/sizeof(t3[0]);

double F3(double* point) {
	double A = point[0];
	double T = point[1];
	double B = point[2]; 
	double result = 0;
	for (int i=0; i<N3; i++) {
		result += pow(A*exp(-t3[i]/T)+B-y3[i],2)/pow(e3[i],2);
	}
	return result;
}

void simplexMinimizer(){
	int d = 2;
	double simplex_size_goal = 1e-6;
	printf("Simplex size goal = %g\n\n",simplex_size_goal);

	double** simplex=(double**)calloc(d+1,sizeof(double*));
	for (int col=0; col<d+1; col++) simplex[col] = (double*)calloc(d,sizeof(double));

	for (int k=0; k<d+1; k++) for (int i=0; i<d; i++)
		simplex[k][i] = 5+(k==i? 6 : 0);

	printf("Minimization of Rosenbrock's valley function:\n");
	printf("Exact minimum: f(1, 1)=0\n");
	printf("Initial simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf("\n");
	}

	downhill_simplex(rosenf3,simplex,d,simplex_size_goal);

	printf("Final simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",rosenf3(simplex[k]));
	}

	free(simplex);
	simplex=(double**)calloc(d+1,sizeof(double*));
	for (int col=0; col<d+1; col++) simplex[col] = (double*)calloc(d,sizeof(double));

	for (int k=0; k<d+1; k++) for (int i=0; i<d; i++)
		simplex[k][i] = 5+(k==i? 6 : 0);

	printf("\n\nMinimization of Himmelblau's function:\n");
	printf("Exact minimum 1: f(3, 2)=0\n");
	printf("Exact minimum 2: f(-2.805, 3.131)=0\n");
	printf("Exact minimum 3: f(-3.779, -3.283)=0\n");
	printf("Exact minimum 4: f(3.584, -1.848)=0\n");
	printf("Initial simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf("\n");
	}

	downhill_simplex(himmelf3,simplex,d,simplex_size_goal);

	printf("Final simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",himmelf3(simplex[k]));
	}

	d = 3;
	free(simplex);
	simplex=(double**)calloc(d+1,sizeof(double*));
	for (int col=0; col<d+1; col++) simplex[col] = (double*)calloc(d,sizeof(double));

	for (int k=0; k<d+1; k++) for (int i=0; i<d; i++)
		simplex[k][i] = 5+(k==i? 6 : 0);

	printf("\n\nNon-linear least-squares fitting problem:\n");
	printf("Initial simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf("\n");
	}

	downhill_simplex(F3,simplex,d,simplex_size_goal);

	printf("Final simplex:\n");
	for (int k=0; k<d+1; k++) {
		for (int i=0; i<d; i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",F3(simplex[k]));
	}
}
