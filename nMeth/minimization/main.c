#include<stdio.h>
#include"minimizers.h"

int main() {
	printf("Newton minimization: \n__________________________\n\n");
       	newtonMinimizer();
	fprintf(stderr,"\n\n");

	printf("\nQuasi-Newton minimization with Broyden's update:\n________________________ \n\n");
	qNewtonMinimizer();

	fitProblem();

	printf("\n\nDownhill simplex minimization:\n______________________________ \n\n");
	simplexMinimizer();
}
