#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multiroots.h>
#include"multirootGSL.h"
#define FMT "%10.4g"

void rootFindGSL() {
	// starting condition for all systems
	double x_init[2] = {2.0, -3.0};

	printf(" Solving system of linear equations: \n"); 
	// A1: Solve system of equations
	double A = 10000;
	int ncalls = 0;
	int eqsystGSL(const gsl_vector* p, void* params, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, A*x*y-1);
		gsl_vector_set(fx,1, exp(-x)+exp(-y)-1-1.0/A);
		return GSL_SUCCESS;
	}

	const size_t n = 2;
	double* par = NULL;
	gsl_multiroot_function eqsystF = {&eqsystGSL, n, &par};
	
	gsl_vector* x = gsl_vector_alloc (n);
	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);
	
	multirootGSL(eqsystF, n, x);
	fprintf(stderr,"\t1\n");
	printf("\n Number of calls to f: %i\n",ncalls);
	fprintf(stderr,"%i\t1\n",ncalls);
	
	gsl_vector_free (x);

	printf("\n");

	// Find minimum of Rosenbrock
	printf("\n Extremum of Rosenbrock's valley function: \n");
	ncalls = 0;

	int rosenfGSL(const gsl_vector* p, void* params, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+200*(y-x*x)*(-2*x));
		gsl_vector_set(fx,1, 200*(y-x*x));
		return GSL_SUCCESS;
	}
	par = NULL;
	gsl_multiroot_function rosenF = {&rosenfGSL, n, &par};

	x = gsl_vector_alloc (n);
	gsl_vector_set (x, 0, x_init[0]); 
	gsl_vector_set (x, 1, x_init[1]);

	multirootGSL(rosenF, n, x);
	fprintf(stderr,"\t2\n");
	printf("\n Number of calls to f: %i\n",ncalls);
	fprintf(stderr,"%i\t2\n",ncalls);
	
	gsl_vector_free(x);
	printf("\n");

	// Find minimum of Himmelblau's function
	printf("\n Extremum of Himmelblau's function:\n");
	ncalls = 0;
	int himmelfGSL(const gsl_vector* p, void* params, gsl_vector* fx) {
		ncalls++;
		double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));
		gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
		return GSL_SUCCESS;
	}
	par = NULL;

	gsl_multiroot_function himmelF = {&himmelfGSL, n, &par};

	x = gsl_vector_alloc (n);
	gsl_vector_set (x, 0, x_init[0]); 
	gsl_vector_set (x, 1, x_init[1]);

	multirootGSL(himmelF, n, x);
	fprintf(stderr,"\t3\n");
	printf("\n Number of calls to f: %i\n",ncalls);
	fprintf(stderr,"%i\t3\n",ncalls);
	gsl_vector_free(x);
}
