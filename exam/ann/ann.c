#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include"ann.h"

int qnewton( double f(gsl_vector* x), gsl_vector* x, double acc );

ann* annAlloc(int numberOfHiddenNeurons, double(*g)(double), double(*dg)(double)) { /* Initialize ANN */
	ann* network = malloc(sizeof(ann));
	network->n = numberOfHiddenNeurons;
	network->g = g;
	network->dg = dg;
	network->data = gsl_vector_alloc(numberOfHiddenNeurons*3);
	return network;
}	

void annFree(ann* network) { /* Free ANN */
	gsl_vector_free(network->data);
	free(network);
}

gsl_vector* annFeedForward(ann* network, double x) {
	double result_F = 0;
	double result_dF = 0;
	gsl_vector* FdF = gsl_vector_alloc(2);
	for(int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data,3*i+0);
		double bi = gsl_vector_get(network->data,3*i+1);
		double wi = gsl_vector_get(network->data,3*i+2);
		result_F += wi*(network->g((x-ai)/bi));
		result_dF += network->dg((x-ai)/bi)*wi/bi;
	}
	gsl_vector_set(FdF,0,result_F);
	gsl_vector_set(FdF,1,result_dF);
	return FdF;
}

void annTrain(ann* network, gsl_vector* xlist, double(*fxy)(double,double), double x0, double y0) {
	double delta(gsl_vector* p) {
		gsl_vector_memcpy(network->data,p);
		int N = xlist->size;
		double result = 0;
		gsl_vector* FdF = annFeedForward(network,x0);
		double F_x0 = gsl_vector_get(FdF,0);
		result += N*pow(fabs(F_x0-y0),2);
		for(int i=0; i<N; i++) {
			double x = gsl_vector_get(xlist,i);
			gsl_vector* FdF = annFeedForward(network,x);
			double F_x = gsl_vector_get(FdF,0);
			double dF_x = gsl_vector_get(FdF,1);
			result += pow(fabs(dF_x-fxy(x,F_x)),2);
		}
		return result/N;
	}
	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);

	double acc = 1e-5;
	
	fprintf(stderr,"Quasi-Newton minimization with Broyden's update:\n");
	int steps = qnewton(delta,p,acc);
	fprintf(stderr,"acc = %.1e\n",acc);
	fprintf(stderr,"\nSteps for minimization:%i\n\n\n",steps);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}
