#include<gsl/gsl_multiroots.h>

int multirootGSL(gsl_multiroot_function F, size_t n, gsl_vector* x) {
	int print_state (size_t iter, gsl_multiroot_fsolver * s) {
		fprintf (stdout,"iter = %3lu x = % .3f % .3f "
		"f(x) = % .3e % .3e\n",
		iter,
		gsl_vector_get (s->x, 0),
		gsl_vector_get (s->x, 1),
		gsl_vector_get (s->f, 0),
		gsl_vector_get (s->f, 1));
		return GSL_SUCCESS;
	}
	int iter = 0;
	int status;
	
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* s = gsl_multiroot_fsolver_alloc (T, 2);
	gsl_multiroot_fsolver_set (s, &F, x);
	
	print_state (iter, s);
	
	do
	  {
	    iter++;
	    status = gsl_multiroot_fsolver_iterate (s);
	
	    if (status)   /* check if solver is stuck */
	      break;
	
	    status =
	      gsl_multiroot_test_residual (s->f, 1e-6);
	  }
	while (status == GSL_CONTINUE && iter < 1000);
	print_state (iter, s);
	fprintf(stderr,"%i",iter);
	printf ("status = %s\n", gsl_strerror (status));

	gsl_multiroot_fsolver_free (s);
	return status;
}

