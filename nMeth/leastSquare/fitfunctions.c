#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double fitfunctions1(int i, double x) {
	switch(i) {
		case 0: return log(x); break;
		case 1: return 1.0; break;
		case 2: return x; break;
		default: {fprintf(stderr,"fitfunctions: wrong i:%d\n",i); return NAN;}
	}
}

double fitfunctions2(int i, double x) {
	switch(i) {
		case 0: return 1/x; break;
		case 1: return 1.0; break;
		case 2: return x; break;
		default: {fprintf(stderr,"fitfunctions: wrong i:%d\n",i); return NAN;}
	}
}

double polfit(int i, double x) {
	switch(i) {
		case 0: return 1; break;
		case 1: return x; break;
		case 2: return x*x; break;
		default: {fprintf(stderr,"fitfunctions: wrong i:%d\n",i); return NAN;}
	}
}

