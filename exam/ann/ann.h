/* Struct for storing ANN */
typedef struct {
	int n;
	double (*g)(double);
	double (*dg)(double);
	gsl_vector* data;
} ann;

ann* annAlloc(int numberOfHiddenNeurons, double(*g)(double), double(*dg)(double));
void annFree(ann* network);
gsl_vector* annFeedForward(ann* network, double x);
void annTrain(ann* network, gsl_vector* xlist, double(*fxy)(double,double), double x0, double y0);
