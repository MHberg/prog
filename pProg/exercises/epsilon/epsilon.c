#include<limits.h>
#include<float.h>
#include<stdio.h>
#include "customfunctions.h"

int main() {
	// max integer testing
	// while loop
	printf("\nMaximum integer testing\n");
	int i = 1;
	while(i+1>i) {i++;}
	printf("my max int while loop = %i\n",i);
	
	// for loop
	for(i=1;i+1>i; i++);
	printf("my max int for loop = %i\n",i);
	
	// do-while loop	
	i = 1;
	do
		i++;
	while(i+1>i);
	printf("my max int do-while loop = %i\n",i);

	printf("INT_MAX = %i\n",INT_MAX);


	// minimum integer testing
	// while loop
	printf("\nMinimum integer testing\n");
	int j = -1;
	while(j-1<j) {j--;}
	printf("my min int while loop = %i\n",j);

	// for loop
	for(j=-1; j-1<j; j--);
	printf("my min int for loop = %i\n",j);

	// do-while loop
	j = -1;
	do
		j--;
	while(j-1<j);
	printf("my min int do-while loop = %i\n",j);

	printf("INT_MIN = %i\n",INT_MIN);

	// Machine epsilon testing for float, double and long double using
	// while loops, for loops and do-while loops in that order
	printf("\nMachine epsilon testing for float, double and long double using different iteration techniques\n");
	printf("While loops\n");
	float xf = 1;
	while(1+xf!=1) {xf/=2;}
	printf("machine epsilon float while loop = %g\n",xf*=2);

	double xd = 1;
	while(1+xd!=1) {xd/=2;}
	printf("machine epsilon double while loop = %lg\n",xd*=2);

	long double xld = 1L;
	while(1+xld!=1) {xld/=2;}
	printf("machine epsilon long double while loop = %Lg\n",xld*=2);

	printf("For loops\n");
	float yf = 1;
	for(;1+yf!=1;yf/=2);
	printf("machine epsilon float for loop = %g\n",yf*=2);

	double yd = 1;
	for(;1+yd!=1;yd/=2);
	printf("machine epsilon double for loop = %lg\n",yd*=2);

	long double yld = 1L;
	for(;1+yld!=1;yld/=2);
	printf("machine epsilon long double for loop = %Lg\n",yld*=2);

	printf("Do-while loops\n");
	float zf = 1;
	do
		zf/=2;
	while(1+zf!=1); 
	printf("machine epsilon float do-while loop = %g\n",zf*=2);

	double zd = 1;
	do 
		zd/=2;
	while(1+zd!=1); 
	printf("machine epsilon double do-while loop = %lg\n",zd*=2);

	long double zld = 1L;
	do
		zld/=2;
	while(1+zld!=1);
	printf("machine epsilon long double do-while loop = %Lg\n",zld*=2);

	printf("FLT_EPSILON = %g\n",FLT_EPSILON);
	printf("DBL_EPSILON = %lg\n",DBL_EPSILON);
	printf("LDBL_EPSILON = %Lg\n",LDBL_EPSILON);

	// Test of harmonic sums
	// Test with float
	printf("\nTest of harmonic sums\n");
	printf("Float:\n");
	int max=INT_MAX/2;
	float sum_up_float;
	for(int i = 1; i<max; i++) {
		sum_up_float += 1.0f/i;
	}
	printf("sum_up_float = %g\n",sum_up_float);

	float sum_down_float;
	for(int i = max; i>0; i--) {
		sum_down_float += 1.0f/i;
	}
	printf("sum_down_float = %g\n\n",sum_down_float);	
	printf("Difference in these numbers is caused by the fact that adding values that are more alike in magnitude is more precise than adding values of very different magnitude. Hence the second result should be most correct.\n\n");
	printf("It is the harmonic series which does not converge but since we are calculating in on a computer for a finite number i_n = max the series of course converge. The two sums do not converge to the same number. If max goes up the difference increases.\n");

	printf("\nDouble:\n");
	// Test with double
	double sum_up_double;
	for(int i = 1; i<max; i++) {
		sum_up_double+= 1.0/i;
	}
	printf("sum_up_double= %.15lg\n",sum_up_double);

	double sum_down_double;
	for(int i = max; i>0; i--) {
		sum_down_double+= 1.0/i;
	}
	printf("sum_down_double= %.15lg\n",sum_down_double);	
	printf("\nThe results are much closer since the double is more precise so we do not lose a lot of digits when adding the numbers. We can also go much closer to max without the values go to zero. \n");

	int isEqual = equal(2.0,2.1,0.11,0.3);
	int isNotEqual = equal(2.0,2.1,0.005,0.006);
	printf("\nQuick test of equal function");
	printf("\nisEqual = %i, isNotEqual = %i\n",isEqual,isNotEqual);
}


