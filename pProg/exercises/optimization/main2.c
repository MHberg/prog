#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<math.h>
#include<assert.h>

struct experimental_data {int n; double *t,*y,*e;};

double function_to_minimize (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
	#define f(t) A*exp(-(t)/T) + B
	for(int i=0;i<n;i++) sum += pow( (f(t[i]) - y[i] )/e[i] ,2);
	return sum;
}

int main() {
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

	assert( n==sizeof(y)/sizeof(y[0]) && n==sizeof(e)/sizeof(e[0]));
	fprintf(stderr,"number of experimental points: %i\n",n);

	printf("# t[i], y[i], e[i]\n");
	
	for(int i=0;i<n;i++) printf("%g %g %g\n",t[i],y[i],e[i]);
	printf("\n\n");

	struct experimental_data par; 
	par.n = n;
	par.t = t;
	par.y = y;
	par.e = e;

	const gsl_multimin_fminimizer_type *Type = 
		gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *state = NULL;
	gsl_vector *step, *start;
	gsl_multimin_function F;
	size_t iter = 0;
	int status;
	double size;

	F.n = 3;
	F.f = function_to_minimize;
	F.params = (void*) &par;
	
	//start
	start = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, 5.0);
	gsl_vector_set(start, 1, 3.0);
	gsl_vector_set(start, 2, 4.0);

	//step
	step = gsl_vector_alloc(F.n);
	gsl_vector_set_all(step, 2.0);


	state = gsl_multimin_fminimizer_alloc(Type, F.n);
	gsl_multimin_fminimizer_set(state, &F, start, step);

	do
	{
		iter++;
		status = gsl_multimin_fminimizer_iterate(state);

		if (status) {
			fprintf(stderr, "unable to improve\n");
			break;
		}

		double acc = 0.01;
		size = gsl_multimin_fminimizer_size(state);
		status = gsl_multimin_test_size(size, acc);

		if (status == GSL_SUCCESS)
		{
			fprintf(stderr,"converged to minimum at\n");
		}

		fprintf(stderr,"%5ld %10.3e %10.3e %10.3e f() = %7.3f size = %.3f\n",
				iter,
				gsl_vector_get(state->x,0),
				gsl_vector_get(state->x,1),
				gsl_vector_get(state->x,2),
				state->fval,size);
	}
	while (status == GSL_CONTINUE && iter < 100);

	double A = gsl_vector_get(state->x, 0);
	double T = gsl_vector_get(state->x, 1);
	double B = gsl_vector_get(state->x, 2);

	printf("# Fitted values\n");
	printf("# A\t\tT\tB\n");
	printf("# %.5lg\t%.5lg\t%.5lg\n", A, T, B);
	printf("# Thus estimated lifetime is %.5lg\n",T);
	printf("# t\t A*exp(-t/T+B)\n");
	double dt = (t[n-1]-t[0])/50;
	for (double ti = t[0]; ti<t[n-1]+dt; ti+=dt) {
		printf("%g\t%g\n",ti,f(ti));
	}

	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	
	return status;
}
