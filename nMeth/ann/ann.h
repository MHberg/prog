typedef struct {
	int n;
	double (*f)(double);
	gsl_vector* data;
} ann;

ann* annAlloc(int numberOfHiddenNeurons, double(*activationFunction)(double));
void annFree(ann* network);
double annFeedForward(ann* network, double x);
void annTrain(ann* network, gsl_vector* xlist, gsl_vector* ylist);
