#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include"newton.h"

double rosenf(gsl_vector* v) {
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmelf(gsl_vector* v) {
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

void qNewtonMinimizer() {
	int n = 2, steps;
	gsl_vector* v = gsl_vector_alloc(n);
	double eps = 1e-3;
	printf("eps = %.1e\n",eps);

	printf("\nMinimization of Rosenbrock's valley function:\n");
	printf("Start point: \n");
	gsl_vector_set(v,0,-3);
	gsl_vector_set(v,1,5);
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (start): %g\n",rosenf(v));
	steps = qnewton(rosenf,v,eps);
	printf("Steps = %i\n", steps);
	fprintf(stderr,"%i\t2\n",steps);
	printf("Minimum of function found at: \n");
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (min): %g\n",rosenf(v));

	printf("\nMinimization of Himmelblau's function:\n");
	printf("Start point: \n");
	gsl_vector_set(v,0,-3);
	gsl_vector_set(v,1,5);
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (start): %g\n",himmelf(v));
	steps = qnewton(himmelf,v,eps);
	printf("Steps = %i\n", steps);
	fprintf(stderr,"%i\t3\n",steps);
	printf("Minimum of function found at: \n");
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (min): %g\n",himmelf(v));
}
