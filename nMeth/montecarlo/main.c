#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mc.h"

//For part C
double adapt2D(double f(double* x), double a, double b, double c(double x), double d(double x), double acc, double eps, double* err);

/* Expects up to two arguments. First argument determines number of sample points N, if not specified defaults to 1e6. 
 * Second argument determines if the difficult singular integral should be computed - defaults to 0 (do not compute).*/
int main(int argc, char** argv) {
	int N = (argc>1 ? atoi(argv[1]) : 1e6);
	int calls = 0;
	double f1(double* x) {
		calls++;
		return cos(x[0])/sqrt(x[0]+1);
	}
	int dim = 1;
	double a1[] = {-1}; double b1[] = {1};
	double result, err;
	double expected = 2.20742; 
	plainmc(dim, a1, b1, f1, N, &result, &err);
	printf("\nPart A (first three integrals also used for error estimates for part B which can be seen on the plot):\n");
	printf("\nIntegral of cos(x)/sqrt(x+1) from -1 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",result,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(result-expected));
	fprintf(stderr,"%i\t%lg\t",N,err);

	calls = 0;
	dim = 2;
	double f2(double* x) {
		calls++;
		return x[0]*x[0]*sin(x[1]); 
	}
	double a2[] = {0,0}; double b2[] = {1,M_PI};
	expected = 2./3; 
	plainmc(dim, a2, b2, f2, N, &result, &err);
	printf("\nIntegral of x^2*sin(y) from [0,0] to [1,pi]:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",result,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(result-expected));
	fprintf(stderr,"%lg\t",err);
	
	calls = 0;
	dim = 3;
	double f3(double* x) {
		calls++;
		return sin(x[0])*sin(x[0])+x[1]*sin(x[2]);
	}
	double a3[] = {0,0,0}; double b3[] = {M_PI,1,M_PI};
	expected = 1./2*M_PI*(2+M_PI); 
	plainmc(dim, a3, b3, f3, N, &result, &err);
	printf("\nIntegral of sin(x)^2+y*sin(z) from [0,0,0] to [pi,1,pi]:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",result,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(result-expected));
	fprintf(stderr,"%lg\n",err);

	int doSingularIntegral = (argc>2 ? atoi(argv[2]) : 0);
	if(doSingularIntegral) {
		calls = 0;
		dim = 3;
		double f4(double* x) {
			calls++;
			return 1/(1-cos(x[0])*cos(x[1])*cos(x[2]))/pow(M_PI,3);
		}
		double a4[] = {0,0,0}; double b4[] = {M_PI,M_PI,M_PI};
		expected = 1.3932039296856768591842462603255;
		plainmc(dim, a4, b4, f4, N, &result, &err);
		printf("\nIntegral of 1/(1-cos(x)*cos(y)*cos(z))/pi^3 from [0,0,0] to [pi,pi,pi]:\n");
		printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",result,err,calls);
		printf("Expected = %lg\tActual error = %lg\n",expected,fabs(result-expected));
	}

	// 2D function integral by recursively calling 1D adaptive integrator
	calls = 0;
	double f4(double* x) {
		calls++;
		return x[0]*x[0]*sin(x[1]);
	}
	double c(double x) {
		return sin(x)+1;
	}
	double d(double x) {
		return 2*x-1;
	}
	double a=0, b=1, acc=1e-4, eps=1e-4;
	printf("\n\nPart C:\n");
	printf("Integral of 2D function by recursively calling 1D adaptive integrator:\n");
	printf("\nAbsolute accuracy goal = %g\nRelative accuracy goal = %g\n",acc,eps);
	printf("_______________________________\n");
	double Q = adapt2D(f4, a, b, c, d, acc, eps, &err);
	expected = -0.302923; 
	printf("\nIntegral of x^2*sin(y) from {0,sin(x)+1} to {1,2*x-1}:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	double f5(double* x) {
		calls++;
		return x[0]*x[0]*x[0]+cos(x[1])*exp(x[0])/sqrt(x[1]+1);
	}
	double g(double x) {
		return 1./(x+1);
	}
	double h(double x) {
		return 1./2*x*x;
	}
	a=0, b=2, acc=1e-4, eps=1e-4;
	Q = adapt2D(f5, a, b, g, h, acc, eps, &err);
	expected = 4.94451; 
	printf("\nIntegral of x³+exp(x)*cos(y)/sqrt(1+y) from {0,1/(x+1)} to {2,1/2*x²}:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));
}
