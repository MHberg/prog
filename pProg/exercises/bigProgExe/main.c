#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_min.h>

double my_func(double x, void *params) {
	double *p = (double *) params;
	#define f(x) 1.0/4*(x+1.0/x)
	return *p*(x+1.0/x);
}

int main() {
	int status;
	int iter = 0, max_iter = 100;
	const gsl_min_fminimizer_type *T;
	gsl_min_fminimizer *s;
	double m = 0.5, m_expected = 1.0;
	double a = 0.01, b = 5.0;
	gsl_function F;

	F.function = &my_func;
	double my_params = 1.0/4;
	F.params = &my_params;

	T = gsl_min_fminimizer_brent;
	s = gsl_min_fminimizer_alloc(T);
	gsl_min_fminimizer_set(s, &F, m, a, b);

	fprintf(stderr, "using %s method\n",
        gsl_min_fminimizer_name(s));

	fprintf(stderr, "%5s [%9s, %9s] %9s %10s %9s\n",
        "iter", "lower", "upper", "min",
        "err", "err(est)");

	fprintf(stderr, "%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
        iter, a, b,
        m, m - m_expected, b - a);

	printf("#x\tf(x)\n");
	do
	{
		iter++;
		status = gsl_min_fminimizer_iterate(s);

		if (status) {
		       	fprintf(stderr,"iterating failed with status code = %i\n",status);
			break;
		}

		m = gsl_min_fminimizer_x_minimum(s);
		a = gsl_min_fminimizer_x_lower(s);
		b = gsl_min_fminimizer_x_upper(s);

		status = gsl_min_test_interval(a, b, 0.001, 0.0);

		if (status == GSL_SUCCESS) {
			fprintf(stderr,"#converged to minimum at \n");
		}
		fprintf(stderr,"%5d [%.7f, %.7f] "
				"%.7f %+.7f %.7f\n",
				iter, a, b,
				m, m - m_expected, b - a);
	}
	while (status == GSL_CONTINUE && iter < max_iter);
	printf("%lg\t%lg\n", m, f(m));

	gsl_min_fminimizer_free(s);
	
	printf("\n\n#x\tf(x)\n");
	for(double xi = 0.01; xi<=5; xi+=0.01) {
		printf("%lg\t%lg\n",xi,f(xi));
	}

	return status;
}
