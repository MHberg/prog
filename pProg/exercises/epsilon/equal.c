#include<math.h>

int equal(double a, double b, double tau, double epsilon) {
	int absolutePrecision = fabs(a-b) < tau;
	int relativePrecision = fabs(a-b)/(fabs(a)+fabs(b)) < epsilon/2;
	if (absolutePrecision || relativePrecision)
		return 1;
	else
		return 0;
}
	
