#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include"ann.h"
#include"newton.h"

ann* annAlloc(int numberOfHiddenNeurons, double(*activationFunction)(double)) {
	ann* network = malloc(sizeof(ann));
	network->n = numberOfHiddenNeurons;
	network->f = activationFunction;
	network->data = gsl_vector_alloc(numberOfHiddenNeurons*3);
	return network;
}	

void annFree(ann* network) {
	gsl_vector_free(network->data);
	free(network);
}

double annFeedForward(ann* network, double x) {
	double result = 0;
	for(int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data,3*i+0);
		double bi = gsl_vector_get(network->data,3*i+1);
		double wi = gsl_vector_get(network->data,3*i+2);
		result += network->f((x-ai)/bi)*wi;
	}
	return result;
}

void annTrain(ann* network, gsl_vector* xlist, gsl_vector* fxlist) {
	double delta(gsl_vector* p) {
		gsl_vector_memcpy(network->data,p);
		double result = 0;
		for(int i=0; i<xlist->size; i++) {
			double x = gsl_vector_get(xlist,i);
			double fx = gsl_vector_get(fxlist,i);
			double y = annFeedForward(network,x);
			result += pow(y-fx,2);
		}
		return result/xlist->size;
	}
	int steps;
	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
	double eps = 1e-3;
	
	fprintf(stderr,"eps = %.1e\n",eps);
	steps = qnewton(delta,p,eps);
	fprintf(stderr,"\nSteps for minimization:%i\n\n\n",steps);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}
