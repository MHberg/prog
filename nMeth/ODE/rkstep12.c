#include<gsl/gsl_vector.h>
#include<stdio.h>

void rkstep12(
	double x,                                  /* the current value of the variable */
	double h,                                  /* the step to be taken */
	gsl_vector* yx,                                /* the current value y(x) of the sought function */
	void f(double x, gsl_vector* y, gsl_vector* dydx), /* the right-hand-side, dydt = f(x,y) */
	gsl_vector* yxh,                               /* output: y(x+h) */
	gsl_vector* err                                /* output: error estimate dy */
) {
	int n = yx->size, i;
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* yt = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);

	f(x,yx,k0);
	for (i=0; i<n; i++) {
		gsl_vector_set(yt,i,gsl_vector_get(yx,i)+gsl_vector_get(k0,i)*h/2);
	}
	f(x+h/2,yt,k12);
	for (i=0; i<n; i++) {
		gsl_vector_set(yxh,i,gsl_vector_get(yx,i)+gsl_vector_get(k12,i)*h);
	}
	for (i=0; i<n; i++) {
		gsl_vector_set(err,i,(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2);
	}
}
