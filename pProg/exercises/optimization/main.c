#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

double f(const gsl_vector *v, void *params) {
	double x, y;
	double *p = (double *) params;
	
	x = gsl_vector_get(v, 0);
	y = gsl_vector_get(v, 1);
	return (1-x)*(1-x)+p[0]*(y-x*x)*(y-x*x);
}

int main() {
	size_t iter = 0;
	int status;
	int dimension = 2;
	double size;

	double par[1] = {100};

	const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *s = NULL;

	gsl_vector *ss, *x;
	gsl_multimin_function func;

	func.n = dimension;
	func.f = f;
	func.params = par;

	ss = gsl_vector_alloc(2);
	gsl_vector_set_all(ss, 1.0);

	x = gsl_vector_alloc(2);
	gsl_vector_set(x, 0, -10.0);
	gsl_vector_set(x, 1, -5.0);

	s = NULL;

	s = gsl_multimin_fminimizer_alloc(T, 2);
	gsl_multimin_fminimizer_set(s, &func, x, ss);

	do
	{
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);
		if (status) break;
		
		size = gsl_multimin_fminimizer_size(s);
		status = gsl_multimin_test_size(size, 1e-2);

		if (status == GSL_SUCCESS) {
			printf("#converged to minimum at \n");
		}
		printf("%5d %10.3e %10.3e f() = %7.3f size = %.3f\n",
				iter,
				gsl_vector_get (s->x, 0),
				gsl_vector_get (s->x, 1),
				s->fval, size);
	}
	while (status == GSL_CONTINUE && iter < 100);

	gsl_vector_free(x);
	gsl_vector_free(ss);
	gsl_multimin_fminimizer_free(s);

	return status;
}

