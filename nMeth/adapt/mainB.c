#include<stdio.h>
#include<math.h>
#include"integrator.h"
#include<gsl/gsl_integration.h>
void mainB() {
	int calls = 0;
	double a = -INFINITY, b = INFINITY;
	double err;
	double acc=1e-14, eps=1e-14;
	printf("\n\nAbsolute accuracy goal = %g\nRelative accuracy goal = %g\n",acc,eps);
	printf("______________________________\n");
	double expected = 0.070707145212848;
	double f6(double x) {
		calls++;
		return exp(-(2*x*x+3*x+4));
	}
	double Q = adapt(f6, a, b, acc, eps, &err);
	printf("\nIntegral of exp(-(ax^2+bx+c)) with a=2, b=3, c=4 from -Inf to Inf (my adaptive integrator):\n");
	printf("Value = %.7lg\tEstimated error = %.7lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %.7lg\tActual error = %.7lg\n",expected,fabs(Q-expected));

	//Library integrator
	calls = 0;
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);
	double result, error;
	gsl_function F;
	F.function = &f6;
	F.params = NULL;
	gsl_integration_qagi(&F, acc, eps, 1000, w, &result, &error);
	printf("\nIntegral of exp(-(ax^2+bx+c)) with a=2, b=3, c=4 from -Inf to Inf (gsl_integration_qagi):\n");
	printf("Value = %.7lg\tEstimated error = %.7lg\tCalls = %i\n",result,error,calls);
	printf("Expected = %.7lg\tActual error = %.7lg\n",expected,fabs(result-expected));

	calls = 0;
	a = 0, b = INFINITY;
	double f7(double x) {
		calls++;
		return exp(-2*x)*cos(x);
	}
	expected = 2.0/5;
	Q = adapt(f7, a, b, acc, eps, &err);
	printf("\nIntegral of exp(-2x)*cos(x) from 0 to Inf (my adaptive integrator):\n");
	printf("Value = %.7lg\tEstimated error = %.7lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %.7lg\tActual error = %.7lg\n",expected,fabs(Q-expected));

	//Library integrator
	calls = 0;
	w = gsl_integration_workspace_alloc(1000);
	F.function = &f7;
	F.params = NULL;
	gsl_integration_qagiu(&F, a, acc, eps, 1000, w, &result, &error);
	printf("\nIntegral of exp(-2x)*cos(x) from 0 to Inf (gsl_integration_qagiu):\n");
	printf("Value = %.7lg\tEstimated error = %.7lg\tCalls = %i\n",result,error,calls);
	printf("Expected = %.7lg\tActual error = %.7lg\n",expected,fabs(result-expected));
	gsl_integration_workspace_free(w);
}
