#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<math.h>
#include"interpolFunctions.h"

int main() {
	int n = 11;
	double x[n];
	double y[n];
	printf("# original data points (index 0)\n# x\ty\n");
	for (int i=0; i<n; i++) {
		x[i] = i*4*M_PI/(n-1);
		y[i] = f(x[i]);
		printf("%g\t%lg\n",x[i],y[i]);
	}

	int N = 100;
	printf("\n\n# x\tlinterp(x)\tsin(x)(index 1)\n");
	for (int j = 0; j<=N; j++) {
		double z = j*4*M_PI/N;
		printf("%lg\t%lg\t%lg\n",z,linterp(n,x,y,z),f(z));
	}
	
	printf("\n\n# x\tlinterp_integ(x)\tsin_integ(x)(index 2)\n");
	for (int j = 0; j<=N; j++) {
		double z = j*4*M_PI/N;
		printf("%lg\t%lg\t%lg\n",z,linterp_integ(n,x,y,z),-cos(z)+cos(0));
	}

	printf("\n\n# x\tqspline(x)\tsin(x)(index 3)\n");
	qspline *s = qspline_alloc(n, x, y);
	for (int j = 0; j<=N; j++) {
		double z = j*4*M_PI/N;
		printf("%lg\t%lg\t%lg\n",z,qspline_eval(s,z),f(z));
	}

	// Qspline with f(x) = 1, f(x) = x and f(x) = x^2
	n = 5;
	double x1[n];
	double y1[n];
	printf("\n\n# original data points(index 4)\n# x\ty\n");
	for (int i=0; i<n; i++) {
		x1[i] = i+1;
		y1[i] = 1; 
		printf("%lg\t%lg\n",x1[i],y1[i]);
	}
#define g1(x) 1.0

	printf("\n\n# x\tqspline(x)\tf(x) = 1(index 5)\n");
	s = qspline_alloc(n, x1, y1);
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		//fprintf(stderr,"x[0] = %lg\n",s->x[4]);
		//fprintf(stderr,"z = %lg\n",z);
		printf("%lg\t%lg\t%lg\n",z,qspline_eval(s,z),g1(z));
	}
	printf("\n\n# x\tqspline_deriv(x)\tf(x)=1_deriv(index 6)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%i\n",z,qspline_derivative(s,z),0);
	}
	printf("\n\n# x\tqspline_integ(x)\tf(x)=1_integ(index 7)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%lg\n",z,qspline_integral(s,z),z-1);
	}

	qspline_free(s);

	double x2[n];
	double y2[n];
	printf("\n\n# original data points(index 8)\n# x\ty\n");
	for (int i=0; i<n; i++) {
		x2[i] = i+1;
		y2[i] = i+1; 
		printf("%lg\t%lg\n",x2[i],y2[i]);
	}
#define g2(x) x

	printf("\n\n# x\tqspline(x)\tf(x)=x(index 9)\n");
	s = qspline_alloc(n, x2, y2);
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		//fprintf(stderr,"x[0] = %lg\n",s->x[4]);
		//fprintf(stderr,"z = %lg\n",z);
		printf("%lg\t%lg\t%lg\n",z,qspline_eval(s,z),g2(z));
	}

	printf("\n\n# x\tqspline_deriv(x)\tf(x)=x_deriv(index 10)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%i\n",z,qspline_derivative(s,z),1);
	}
	printf("\n\n# x\tqspline_integ(x)\tf(x)=1_integ(index 11)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%lg\n",z,qspline_integral(s,z),1.0/2*(z*z-1));
	}

	qspline_free(s);

	double x3[n];
	double y3[n];
	printf("\n\n# original data points(index 12)\n# x\ty\n");
	for (int i=0; i<n; i++) {
		x3[i] = i+1;
		y3[i] = (i+1)*(i+1); 
		printf("%lg\t%lg\n",x3[i],y3[i]);
	}
#define g3(x) x*x

	printf("\n\n# x\tqspline(x)\tf(x)=x^2(index 13)\n");
	s = qspline_alloc(n, x3, y3);
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		//fprintf(stderr,"x[0] = %lg\n",s->x[4]);
		//fprintf(stderr,"z = %lg\n",z);
		printf("%lg\t%lg\t%lg\n",z,qspline_eval(s,z),g3(z));
	}

	printf("\n\n# x\tqspline_deriv(x)\tf(x)=x^2_deriv(index 14)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%lg\n",z,qspline_derivative(s,z),2*z);
	}
	printf("\n\n# x\tqspline_integ(x)\tf(x)=x^2_integ(index 15)\n");
	for (int j = 0; j<=N; j++) {
		double z = 1.0+j*4.0/N;
		printf("%lg\t%lg\t%lg\n",z,qspline_integral(s,z),1.0/3*(z*z*z-1));
	}
	qspline_free(s);
}
