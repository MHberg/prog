#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<assert.h>
#include"qr.h"
#include"ls.h"
#include"fitfunctions.h"

int main(int argv, char** argc) {
	double (*funs)(int i, double x);
	if(argv > 1) {
		switch(atoi(argc[1])) {
			case 0: funs = fitfunctions1; break;
			case 1: funs = fitfunctions2; break;
			case 2: funs = polfit; break;
			default: {fprintf(stderr,"main: function specifier should be between 0 and 2 but is %d, defaulting to 0\n",atoi(argc[1]));}
		}
	}
	else funs = fitfunctions1;
	
	int n = 10;
	double* x = (double *) malloc(sizeof(double)*n);
	double* y = (double *) malloc(sizeof(double)*n);
	double* dy = (double *) malloc(sizeof(double)*n);
	// check that memory allocation did not fail
	assert(x!=NULL);
	assert(y!=NULL);
	assert(dy!=NULL);
	
	//printf("# original data points\n# x\ty\tdy\n");
	int i = 0;
	char temp;
	do {
		scanf("%lf%c",&x[i], &temp);
		i++;
		if (i>n) { 
			n=2*n;
			x = realloc(x,sizeof(double)*n);
			y = realloc(y,sizeof(double)*n);
			dy = realloc(dy,sizeof(double)*n);
		}
	} while (temp != '\n');
	i = 0;
	do {
		scanf("%lf%c",&y[i], &temp);
		i++;
		if (i>n) { 
			n=2*n;
			x = realloc(x,sizeof(double)*n);
			y = realloc(y,sizeof(double)*n);
			dy = realloc(dy,sizeof(double)*n);
		}
	} while (temp != '\n');

	i = 0;
	do {
		scanf("%lf%c",&dy[i], &temp);
		i++;
		if (i>n) { 
			n=2*n;
			x = realloc(x,sizeof(double)*n);
			y = realloc(y,sizeof(double)*n);
			dy = realloc(dy,sizeof(double)*n);
		}
	} while (temp != '\n');
	n = i;
	
	gsl_vector* vx = gsl_vector_alloc(n);
	gsl_vector* vy = gsl_vector_alloc(n);
	gsl_vector* vdy = gsl_vector_alloc(n);

	for (int i = 0; i<n; i++) {
		printf("%g %g %g\n",x[i],y[i],dy[i]);
		gsl_vector_set(vx,i,x[i]);
		gsl_vector_set(vy,i,y[i]);
		gsl_vector_set(vdy,i,dy[i]);
	}
	printf("\n\n");

	int m = 3;
	gsl_vector* c = gsl_vector_alloc(m); //coeffs
	gsl_matrix* S = gsl_matrix_alloc(m,m); //covariance matrix
	lsfit(vx,vy,vdy,m,funs,c,S);

	// estimates of errors on fitting coeffs
	gsl_vector* dc = gsl_vector_alloc(m);
	for (int k=0; k<m; k++) {
		//sqrt of diagonal elements of covariance matrix are estimates of errors on coefficients
		double skk = gsl_matrix_get(S,k,k);
		gsl_vector_set(dc,k,sqrt(skk)); 
	}

	double fit(double x) {
		double s = 0;
		for (int k=0; k<m; k++) {
			s += gsl_vector_get(c,k)*funs(k,x);
		}
		return s;
	}

	double fit_plus(int i, double x) {
		return fit(x)+gsl_vector_get(dc,i)*funs(i,x);
	}

	double fit_minus(int i, double x) {
		return fit(x)-gsl_vector_get(dc,i)*funs(i,x);
	}
	
	// prints out fit including errors on coefficients
	double z, dz = (x[n-1]-x[0])/90;
	for(int i=0; i<m; i++) {
		z = x[0]-dz/2;
		do {
			printf("%g %g %g %g\n",z,fit(z),fit_plus(i,z),fit_minus(i,z));
			z += dz;
		} while(z < x[n-1]+dz);
		printf("\n\n");
	}
}
