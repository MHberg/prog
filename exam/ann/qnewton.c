#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#define EPS (1e-8)

void numerical_grad(double f(gsl_vector* x), gsl_vector* x, gsl_vector* gradient) {
	double fx = f(x);
	for (int i=0; i<x->size; i++) {
		double xi = gsl_vector_get(x,i);
		double dx = fabs(xi)*EPS;
		if (fabs(xi) < sqrt(EPS)) dx = EPS;
		gsl_vector_set(x,i,xi+dx);
		gsl_vector_set(gradient,i,(f(x)-fx)/dx);
		gsl_vector_set(x,i,xi);
	}
}

int qnewton(
double (*f)(gsl_vector* x), 
gsl_vector* x, /* starting point - this is where the latest approximation to the root is stored on exit */
double acc /* accuracy goal */
) {
	int n = x->size, numberOfSteps = 0;

	gsl_matrix* B = gsl_matrix_alloc(n,n); 
	gsl_vector* gradient = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* gz = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* u = gsl_vector_alloc(n);

	gsl_matrix_set_identity(B); //start with identity matrix as the zeroth approximation for the inverse Hessian matrix
	numerical_grad(f,x,gradient);
	double fx = f(x), fz;
	while(1) {
		numberOfSteps++;
		gsl_blas_dgemv(CblasNoTrans,-1,B,gradient,0,Dx);
		if (gsl_blas_dnrm2(Dx) < EPS*gsl_blas_dnrm2(x)) {
			fprintf(stderr,"qnewton: Dx < EPS*|x|\n");
			break;
		}
		if (gsl_blas_dnrm2(gradient) < acc) {
			fprintf(stderr,"qnewton: |grad| < acc\n");
			break;
		}
		double lambda = 1; //initialize lambda = 1
		while(1) { //backtracking line-search
			gsl_vector_memcpy(z,x); // z = x
			gsl_vector_memcpy(s,Dx); 
			gsl_vector_scale(s,lambda); // step size s
			gsl_vector_add(z,s); // z = z+s = x+s
			fz = f(z); // f(z) = f(x+s)
			double sTgradient; 
			gsl_blas_ddot(s,gradient,&sTgradient); // (s^T)grad(f)
			double alpha = 0.01;
			int armijoCondition = fz < fx+alpha*sTgradient;
			if ( armijoCondition ) break;
			if ( lambda < EPS) { //lambda lower than EPS, reset current inverse Hessian matrix (B) to identity matrix.
				gsl_matrix_set_identity(B);
				break;
			}
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
		}

		numerical_grad(f,z,gz); //grad(f(z)) = grad(f(x+s))
		gsl_vector_memcpy(y,gz);
		gsl_blas_daxpy(-1,gradient,y); // here y = grad(z) - grad(x)
		gsl_vector_memcpy(u,Dx); // u=s=Dx
		gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); // u = s-By
		double sTy;
		gsl_blas_ddot(Dx,y,&sTy); // (s^T)y
		if (fabs(sTy) > 1e-12) { 
			gsl_blas_dger(1.0/sTy,u,Dx,B); // B = u(s^T) / ((s^T)y) + B = c(s^T) + B (eq. 13+14)
		}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(gradient,gz);
		fx = fz;
	}
	gsl_matrix_free(B);
	gsl_vector_free(gradient);
	gsl_vector_free(Dx);
	gsl_vector_free(s);
	gsl_vector_free(z);
	gsl_vector_free(gz);
	gsl_vector_free(y);
	gsl_vector_free(u);
	return numberOfSteps;
}
