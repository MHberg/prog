#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>

nvector* nvector_alloc(int n) {
	nvector* v = malloc(sizeof(nvector));
	(*v).size = n;
	(*v).data = malloc(n*sizeof(double));
	if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
	return v;
}

void nvector_free(nvector* v) { free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value) { (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

double nvector_dot_product(nvector* u, nvector* v) {
	if ((*u).size != (*v).size) {
		return 0;
	}
	double result = 0;
	for (int i=0; i<(*u).size; i++) {
		result += nvector_get(u,i)*nvector_get(v,i);
	}
	return result;
}	

int nvector_equal(nvector* u, nvector* v) {
	if ((*u).size != (*v).size) {
		return 0;
	}
	int result;
	for (int i=1; i<(*u).size; i++) {
		result = nvector_get(u,i) == nvector_get(v,i);
		if (result != 1) return result;
	}
	return 1;
}

void nvector_add(nvector* a, nvector* b) {
	if ((*a).size != (*b).size) {
		return;
	}
	for (int i=1; i<(*a).size; i++) {
		nvector_set(a, i, nvector_get(a, i) + nvector_get(b, i));
	}
}

void nvector_print(char* s, nvector* v) {
	int n = (*v).size;
	printf("\n%s ( ",s);
	for (int i = 1; i<n; i++) {
		printf("%lg ",nvector_get(v,i));
	}	
	printf(")\n");
}
