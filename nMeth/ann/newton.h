int newton( double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double acc );

int qnewton( double f(gsl_vector* x), gsl_vector* x, double acc );
