#include <stdlib.h>
#include <stdio.h>
#include "simplex.h"

int downhill_simplex(
	double F(double*), double** simplex, int d, double simplex_size_goal)
{
	int hi, lo, k = 0; 
	double centroid[d], F_value[d+1], p1[d], p2[d];
	simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid);
	while (size(simplex,d)>simplex_size_goal) {
		simplex_update(simplex,F_value,d,&hi,&lo,centroid);
		reflection(simplex[hi],centroid,d,p1); 
		double f_re = F(p1);
		if (f_re<F_value[lo]) {
			expansion(simplex[hi],centroid,d,p2);
			double f_ex = F(p2);
			if (f_ex<f_re) {
				for (int i=0; i<d; i++) {
					simplex[hi][i] = p2[i]; 
					F_value[hi]=f_ex;
				}
			}
			else {
				for (int i=0; i<d; i++) {
					simplex[hi][i] = p1[i]; 
					F_value[hi] = f_re;
				}
			}
		}
		else {
			if (f_re<F_value[hi]) {
				for (int i=0; i<d; i++) {
					simplex[hi][i] = p1[i];
					F_value[hi] = f_re; 
				}
			}
			else {
				contraction(simplex[hi],centroid,d,p1); 
				double f_co=F(p1);
				if (f_co<F_value[hi]) {
					for (int i=0;i<d;i++) {
						simplex[hi][i]=p1[i]; 
						F_value[hi]=f_co; 
					}
				}
				else {
					reduction(simplex,d,lo);
					simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid);
				}
			}
		}
		k++;
	} 
	return k;
}
