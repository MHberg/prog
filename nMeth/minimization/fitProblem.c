#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"newton.h"

double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = sizeof(t)/sizeof(t[0]);

double F(gsl_vector* v) {
	double A = gsl_vector_get(v,0);
	double T = gsl_vector_get(v,1);
	double B = gsl_vector_get(v,2);
	double result = 0;
	for (int i=0; i<N; i++) {
		result += pow(A*exp(-t[i]/T)+B-y[i],2)/pow(e[i],2);
	}
	return result;
}

void fitProblem() {
	int n = 3, steps;
	gsl_vector* v = gsl_vector_alloc(n);
	double eps = 1e-3;
	printf("eps = %.1e\n",eps);
	printf("\nNon-linear least-squares fitting problem:\n");
	printf("Start values: \n");
	gsl_vector_set(v,0,5);
	gsl_vector_set(v,1,1);
	gsl_vector_set(v,2,0);
	printf("A =\t%g\n",gsl_vector_get(v,0));
	printf("T =\t%g\n",gsl_vector_get(v,1));
	printf("B =\t%g\n",gsl_vector_get(v,2));
	printf("Function value (start): %g\n",F(v));
	steps = qnewton(F,v,eps);
	printf("Steps = %i\n", steps);
	printf("Minimum of function found at: \n");
	printf("A =\t%g\n",gsl_vector_get(v,0));
	printf("T =\t%g\n",gsl_vector_get(v,1));
	printf("B =\t%g\n",gsl_vector_get(v,2));
	printf("Function value (min): %g\n",F(v));

	fprintf(stderr,"\n\n");
	for (int i=0; i<N; i++) {
		fprintf(stderr,"%g\t%g\t%g\n",t[i],y[i],e[i]);
	}
	
	fprintf(stderr,"\n\n");
	int npoints = 100;
	for (int i=0; i<npoints+1; i++) {
		double ti = i*t[N-1]/npoints;
		fprintf(stderr,"%g\t%g\n",ti,gsl_vector_get(v,0)*exp(-ti/gsl_vector_get(v,1))+gsl_vector_get(v,2));
	}
}
