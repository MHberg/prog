#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include"ann2d.h"
#include"newton.h"

ann2d* annAlloc2d(int numberOfHiddenNeurons, double(*activationFunction)(double,double)) {
	ann2d* network = malloc(sizeof(ann2d));
	network->n = numberOfHiddenNeurons;
	network->f = activationFunction;
	network->data = gsl_vector_alloc(numberOfHiddenNeurons*5);
	return network;
}	

void annFree2d(ann2d* network) {
	gsl_vector_free(network->data);
	free(network);
}

double annFeedForward2d(ann2d* network, double x, double y) {
	double result = 0;
	for(int i=0; i<network->n; i++) {
		double ai = gsl_vector_get(network->data,5*i+0);
		double bi = gsl_vector_get(network->data,5*i+1);
		double ci = gsl_vector_get(network->data,5*i+2);
		double di = gsl_vector_get(network->data,5*i+3);
		double wi = gsl_vector_get(network->data,5*i+4);
		result += network->f((x-ai)/bi,(y-ci)/di)*wi;
	}
	return result;
}

void annTrain2d(ann2d* network, gsl_vector* xlist, gsl_vector* ylist, gsl_matrix* fxylist) {
	double delta(gsl_vector* p) {
		gsl_vector_memcpy(network->data,p);
		double result = 0;
		for(int i=0; i<xlist->size; i++) for(int j=0; j<ylist->size; j++) {
			double x = gsl_vector_get(xlist,i);
			double y = gsl_vector_get(ylist,j);
			double fxy = gsl_matrix_get(fxylist,i,j);
			double u = annFeedForward2d(network,x,y);
			result += pow(u-fxy,2);
		}
		return result/(xlist->size*ylist->size);
	}
	int steps;
	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
	double eps = 1e-3;
	fprintf(stderr,"eps = %.1e\n",eps);
	steps = qnewton(delta,p,eps);
	fprintf(stderr,"Steps for minimization:%i\n",steps);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);	
	
	//size_t iter = 0;
	//int status;
	//double size;
	//const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
	//gsl_multimin_fminimizer *s = NULL;
	//gsl_vector *ss; 
	//gsl_multimin_function minex_func;
	//ss = gsl_vector_alloc(network->data->size);
	//gsl_vector_set_all(ss,1.0);
	//minex_func.n = network->data->size;
	//minex_func.f = &delta;
	//minex_func.params = NULL;
	//s = gsl_multimin_fminimizer_alloc(T,network->data->size);
	//gsl_multimin_fminimizer_set(s,&minex_func, p, ss);
	//do
	//{
	//	iter++;
	//	status = gsl_multimin_fminimizer_iterate(s);
	//	//if (status) break;
	//	size = gsl_multimin_fminimizer_size(s);
	//	//status = gsl_multimin_test_size ( size, 1e-14);
	//	//status = s->fval<eps;
	//	if (s->fval <= eps)
	//	{
	//		fprintf (stderr,"converged to minimum at\n");
	//	}

	//        fprintf (stderr,"%5d %10.3e f() = %7.3f size = %.3f\n",
        //        iter,
        //        gsl_vector_get (s->x, 0),
        //        s->fval, size);
	//}
	//while (status == GSL_CONTINUE && iter < 100000);
	//	fprintf(stderr,"hehe\n f(s) = %g\n",delta(s->x));
	//fprintf(stderr,"eps = %g\n",eps);

	//gsl_vector_free(ss);
	//gsl_multimin_fminimizer_free (s);



}
