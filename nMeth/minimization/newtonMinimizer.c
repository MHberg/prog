#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include"newton.h"

double rosen(gsl_vector* v, gsl_vector* df, gsl_matrix* H){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	gsl_vector_set(df,0,2*(x-1)+200*x*(x*x-y));
	gsl_vector_set(df,1,200*(y-x*x));
	gsl_matrix_set(H,0,0, 2 - 200*y + 600*x*x);
	gsl_matrix_set(H,0,1, -200);
	gsl_matrix_set(H,1,0, -400*x);
	gsl_matrix_set(H,1,1, 200);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmel(gsl_vector* v, gsl_vector* df, gsl_matrix* H){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	gsl_vector_set(df,0,4*x*(x*x+y-11)+2*(x+y*y-7));
	gsl_vector_set(df,1,2*(x*x+y-11)+4*y*(x+y*y-7));
	gsl_matrix_set(H,0,0,12*x*x*y-44+2);
	gsl_matrix_set(H,0,1,4*x+4*y);
	gsl_matrix_set(H,1,0,4*x+4*y);
	gsl_matrix_set(H,1,1,2+4*x+12*y*y-28);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}
void newtonMinimizer() {
	int n = 2, steps;
	gsl_vector* v = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	double eps = 1e-3;
	printf("eps = %.1e\n",eps);

	printf("\nMinimization of Rosenbrock's valley function:\n");
	printf("Start point: \n");
	gsl_vector_set(v,0,-3);
	gsl_vector_set(v,1,5);
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (start): %g\n",rosen(v,df,H));
	steps = newton(rosen,v,eps);
	printf("Steps = %i\n", steps);
	fprintf(stderr,"%i\t2\n",steps);
	printf("Minimum of function found at: \n");
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (min): %g\n",rosen(v,df,H));
	printf("Gradient at min:\n");
	gsl_vector_fprintf(stdout,df,"%g");

	printf("\nMinimization of Himmelblau's function:\n");
	printf("Start point: \n");
	gsl_vector_set(v,0,-3);
	gsl_vector_set(v,1,5);
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (start): %g\n",himmel(v,df,H));
	steps = newton(himmel,v,eps);
	printf("Steps = %i\n", steps);
	fprintf(stderr,"%i\t3\n",steps);
	printf("Minimum of function found at: \n");
	gsl_vector_fprintf(stdout,v,"%g");
	printf("Function value (min): %g\n",himmel(v,df,H));
	printf("Gradient at min:\n");
	gsl_vector_fprintf(stdout,df,"%g");
}
