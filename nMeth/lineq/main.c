#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_blas.h>
#include"qr.h"

void printM(gsl_matrix* M) {
	for (int i = 0; i<M->size1; i++) {
		for (int j = 0; j<M->size2; j++) {
			double x = gsl_matrix_get(M,i,j);
			printf("%g\t",x);
		}
		printf("\n");
	}
}

int main() {
	int n = 4;
	int m = 3;
	gsl_matrix *A = gsl_matrix_alloc(n,m);
	for (int i = 0; i<n; i++) {
		for (int j = 0; j<m; j++) {
			gsl_matrix_set(A,i,j,rand() % 10);
		}
	}
	printf("A (random matrix) = \n");
	printM(A);

	gsl_matrix *R = gsl_matrix_calloc(m,m);
	qr_gs_decomp(A,R);

	int result = 1;
	printf("\nR = \n");
	for (int i = 0; i<m; i++) {
		for (int j = 0; j<m; j++) {
			double x = gsl_matrix_get(R,i,j);
			printf("%g\t",x);
			if (i>j && x!=0) {
			       result = 0;
			}	       
		}
		printf("\n");
	}
	if (result == 0) printf("\nNot upper triangular\n");
	else printf("\nUpper triangular\n");

	printf("\nQ = \n");
	printM(A);
	gsl_matrix *QTQ = gsl_matrix_calloc(A->size2,A->size2);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, QTQ);
	printf("\n(Q^T)Q (should be identity matrix) = \n");
	printM(QTQ);
	
	printf("\nQR (should be equal to A) = \n");
	gsl_matrix *QR = gsl_matrix_alloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,QR);
	printM(QR);
	
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(QR);
	gsl_matrix_free(QTQ);

	n = 3;
	gsl_matrix* AA = gsl_matrix_calloc(n,n);
	gsl_matrix* QQ = gsl_matrix_alloc(n,n);
	for (int i = 0; i<n; i++) {
		for (int j = 0; j<n; j++) {
			gsl_matrix_set(AA,i,j,rand() % 10);
		}
	}
	gsl_matrix_memcpy(QQ,AA);
	printf("\nLinear system: \n");
	printf("\nNew random square matrix A: \n");
	printM(QQ);

	gsl_vector* b = gsl_vector_alloc(n);
	for (int i = 0; i<n; i++) {
		gsl_vector_set(b,i,rand() % 10);
	}
	printf("\nb (random right hand vector) = \n");
	gsl_vector_fprintf(stdout,b,"%g");

	gsl_matrix *RR = gsl_matrix_calloc(n,n);
	qr_gs_decomp(QQ,RR);

	gsl_vector* x = gsl_vector_alloc(n);
	qr_gs_solve(QQ, RR, b, x); 

	printf("\nSolution to equation Ax=b is x = \n");
	gsl_vector_fprintf(stdout,x,"%g");

	gsl_blas_dgemv(CblasNoTrans, 1.0, AA, x, 0.0, b);
	printf("\nAx (should equal b) = \n");
	gsl_vector_fprintf(stdout,b,"%g");

	gsl_matrix* BB = gsl_matrix_alloc(n,n);
	qr_gs_inverse(QQ,RR,BB);
	printf("\nB (inverse of A) = \n");
	printM(BB);

	gsl_matrix* AB = gsl_matrix_alloc(n,n);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AA,BB,0.0,AB);
	printf("\nAB (should equal I) = \n");
	printM(AB);
}
