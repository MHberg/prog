#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ann.h"

int main() {
	printf("#Exam project 2018 - Practical Programming and Numerical Methods\n");
	printf("#Mikael Henneberg - 201506504\n");
	printf("#Exam problem: Artificial neural network (ANN) for solving ODE\n\n"); 
	int numberOfNeurons = 10;
	double activationFunction(double x) { //Gaussian wavelet
		return x*exp(-x*x);
	}
	double activationFunctionDerivative(double x) { //Analytic derivative of Gaussian wavelet
		return (1-2*x*x)*exp(-x*x);
	}

	/* Logistic function ODE */
	double logisticFunction(double x, double y) { //Logistic function ODE
		return y*(1-y);
	}

	ann* network = annAlloc(numberOfNeurons, activationFunction, activationFunctionDerivative);
	double a = -5, b = 5; //x-range
	int nx = 20; //number of points 
	gsl_vector* xlist = gsl_vector_alloc(nx); //list of x-values
	double x0 = 0.0; // initial conditions
	double y0 = 0.5;

	for(int i=0; i<nx; i++) { 
		double x = a+(b-a)*(i)/(nx-1);
		gsl_vector_set(xlist,i,x); 
	}

	for(int i=0; i<network->n; i++) {
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data,3*i+1,1);
		gsl_vector_set(network->data,3*i+2,1);
	}

	annTrain(network,xlist,logisticFunction,x0,y0); // train ANN

	double dz = 1.0/64;
	printf("#Logistic function ODE\n");
	printf("#x\tF_p(x)\n");
	for(double z=a; z<=b; z+=dz) {
		gsl_vector* FdF = annFeedForward(network,z);
		printf("%g\t%g\n",z,gsl_vector_get(FdF,0));
	}
	annFree(network);
	gsl_vector_free(xlist);
	printf("\n\n");


	/* Gaussian function ODE */
	double gaussianFunction(double x, double y) { //Gaussian function ODE
		return -x*y;
	}

	network = annAlloc(numberOfNeurons, activationFunction, activationFunctionDerivative);
	a = -5, b = 5; //x-range
	nx = 20; //number of points 
	xlist = gsl_vector_alloc(nx);
	x0 = 0.0;
	y0 = 1;

	for(int i=0; i<nx; i++) {
		double x = a+(b-a)*(i)/(nx-1);
		gsl_vector_set(xlist,i,x);
	}

	for(int i=0; i<network->n; i++) {
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data,3*i+1,1);
		gsl_vector_set(network->data,3*i+2,1);
	}

	annTrain(network,xlist,gaussianFunction,x0,y0);

	printf("#Gaussian function ODE\n");
	printf("#x\tF_p(x)\n");
	dz = 1.0/64;
	for(double z=a; z<=b; z+=dz) {
		gsl_vector* FdF = annFeedForward(network,z);
		printf("%g\t%g\n",z,gsl_vector_get(FdF,0));
	}
	annFree(network);
	gsl_vector_free(xlist);
	printf("\n\n");
}
