#include<complex.h>
#include<math.h>
#include<stdio.h>
int main() 
{
	// test of mathematical functions and complex numbers
	double x = tgamma(5);
	printf("tgamma(5)=%lg\n",x);

	double y = j1(0.5);
	printf("j1(0.5)=%lg\n",y);

	complex double z=csqrt(-2);
	printf("sqrt(-2)=%lg+%lgi\n",creal(z),cimag(z));

	complex double a = cexp(I);
	printf("exp(i)=%lg+%lgi\n",creal(a),cimag(a));

	complex double b = cexp(I*M_PI);
	printf("exp(pi*i)=%lg+%lgi\n",creal(b),cimag(b));

	complex double c = cpow(I,M_E);
	printf("i^e=%lg+%lgi\n",creal(c),cimag(c));

	// test of data types
	float d = 0.1111111111111111111111111111;
	double e = 0.1111111111111111111111111111;
	long double f = 0.1111111111111111111111111111L;
	printf("float=%.25g\ndouble=%.25lg\nlong double=%.25Lg\n",d,e,f);
	return 0;
}
