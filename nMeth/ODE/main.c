#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include"ode.h"

void f1(double x, gsl_vector* y, gsl_vector* dydx) {
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-1*gsl_vector_get(y,0));
}

void f2(double x, gsl_vector* y, gsl_vector* dydx) {
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);
	double eps = 0.01;
	gsl_vector_set(dydx,0,y1);
	gsl_vector_set(dydx,1,1-y0+eps*y0*y0);
}

int main() {
	int n = 2;
	int max = 100000;
	gsl_vector* xlist = gsl_vector_calloc(max);
	gsl_matrix* ylist = gsl_matrix_calloc(max,n);
	double a = 0, b = 8*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
	gsl_vector_set(xlist,0,a);
	gsl_matrix_set(ylist,0,0,0);
	gsl_matrix_set(ylist,0,1,1);
	int k = ode_driver(xlist,ylist,b,h,acc,eps,max,f1);
	printf("# Numerical solution to differential equation (rkstep12):\n");
	printf("# y1'=-y0\n");
	printf("# x\tf(x)\tsin(x) (exact solution):\n");
	for(int i=0; i<k; i++) printf("%0.5g\t%0.5g\t%0.5g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0),sin(gsl_vector_get(xlist,i)));
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);

	printf("\n\n");
	k = 0;
	xlist = gsl_vector_calloc(max);
	ylist = gsl_matrix_calloc(max,n);
	a = 0, b = 196*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
	gsl_vector_set(xlist,0,a);
	gsl_matrix_set(ylist,0,0,1);
	gsl_matrix_set(ylist,0,1,-0.5);
	k = ode_driver(xlist,ylist,b,h,acc,eps,max,f2);
	printf("# Numerical solution to differential equation (rkstep12):\n");
	printf("# u''(t)+u(t)=1+0.01*u(t)^2\n");
	printf("# or rewritten:\n"); 
	printf("# y0' = y1:\n");
	printf("# y1' = 1-y0+0.01*y0*y0:\n");
	printf("# x\tf(x)\n");
	for(int i=0; i<k; i++) printf("%0.5g\t%0.5g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0));
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);
}
