typedef struct {
	int n;
	double (*f)(double,double);
	gsl_vector* data;
} ann2d;

ann2d* annAlloc2d(int numberOfHiddenNeurons, double(*activationFunction)(double, double));
void annFree2d(ann2d* network);
double annFeedForward2d(ann2d* network, double x, double y);
void annTrain2d(ann2d* network, gsl_vector* xlist, gsl_vector* ylist, gsl_matrix* fxylist);
