#include<gsl/gsl_odeiv2.h>
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_errno.h>

int func(double x, const double y[], double dydx[], void *params) {
	double mu = *(double *)params;
	dydx[0] = mu*exp(-x*x);
	return GSL_SUCCESS;
}

int main(int argc, char *argv[]) {
	double xlower, xupper, dx;
	if (argc < 4) {
		printf("Using default values since not enough arguments were specifiec (required: 3)\n");
		xlower = 0;
		xupper = 3;
		dx = 0.3;
	} else {
		xlower = atof(argv[1]);
		xupper = atof(argv[2]);
		dx = atof(argv[3]);
	} 

	double mu = 2/sqrt(M_PI);
	gsl_odeiv2_system sys = {func, NULL, 1, &mu};
	
	double hstart = 1e-6;
	gsl_odeiv2_driver *d = 
		gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45,hstart,1e-6,1e-6);

	//start u(0)
	double u[1] = {0};
	double x = 0;
	printf("#x\t\tu(x)\n");
	//From 0 to xupper
	for (double xi = 0; xi<=xupper; xi+=dx) {
		int status = gsl_odeiv2_driver_apply(d, &x, xi, u);
		
		if (status) {
			fprintf(stderr,"error, return value=%d\n", status);
			break;
		}

		printf("%.5e\t%.5e\n", x, u[0]);
	}
	
	//From 0 to xlower
	//start u(0)
	u[0] = 0;
	x = 0;
	hstart = -1e-6;
	gsl_odeiv2_driver_reset_hstart(d,hstart);
	for (double xi = 0; xi>=xlower; xi-=dx) {
		int status = gsl_odeiv2_driver_apply(d, &x, xi, u);
		
		if (status) {
			fprintf(stderr,"error, return value=%d\n", status);
			break;
		}

		printf("%.5e\t%.5e\n", x, u[0]);
	}
	gsl_odeiv2_driver_free(d);
	
	printf("\n\n#x\terf(x)\n");
	for (double xi = xlower; xi<=xupper; xi+=dx) {
		printf("%lg\t%lg\n",xi,erf(xi));
	}
	return 0;
}
