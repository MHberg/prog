#include<math.h>
#include<stdlib.h>
#define RND ((double) rand()/RAND_MAX)

void randomx (int dim, double* a, double* b, double* x) {
	for(int i=0; i<dim; i++) {
		x[i] = a[i] + RND*(b[i]-a[i]);
	}
}

void plainmc(int dim, double* a, double* b, double f(double* x), int N, double* result, double* error) {
	double V = 1;
	for(int i=0; i<dim; i++) V *= b[i]-a[i];
	double sum = 0, sumOfSquares = 0, fx, x[dim];
	for(int i=0; i<N; i++) {
		randomx(dim, a, b, x);
		fx = f(x);
		sum += fx; 
		sumOfSquares += fx*fx;
	}
	double avg = sum/N; double var = sumOfSquares/N-avg*avg; //equation 4 in note
	*result = avg*V; // eq. 2
	*error = sqrt(var/N)*V; //eq. 3
}
