#include<stdio.h>
#include<math.h>
#include"integrator.h"

void mainAC() {
	int calls = 0;	
	double f1(double x) {
		calls++;
		return sin(x)/sqrt(x+2);
	}
	double a=-1, b=1, err, acc=1e-4, eps=1e-4;
	printf("Absolute accuracy goal = %g\nRelative accuracy goal = %g\n",acc,eps);
	printf("_______________________________\n");
	double Q = adapt(f1, a, b, acc, eps, &err);
	double expected = -0.117896; 
	printf("\nIntegral of sin(x)/sqrt(x+2) from -1 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;	
	Q = clenshaw_curtis(f1, a, b, acc, eps, &err);
	printf("\nClenshaw Curtis: Integral of sin(x)/sqrt(x+2) from -1 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));
	
	calls = 0;
	a = 0; b = 1;
	double f2(double x) {
		calls++;
		return sqrt(x);
	}
	Q = adapt(f2, a, b, acc, eps, &err);
	expected = 2.0/3;
	printf("\nIntegral of sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	Q = clenshaw_curtis(f2, a, b, acc, eps, &err);
	printf("\nClenshaw Curtis: Integral of sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	double f3(double x) {
		calls++;
		return 1/sqrt(x);
	}
	Q = adapt(f3, a, b, acc, eps, &err);
	expected = 2.0;
	printf("\nIntegral of 1/sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	Q = clenshaw_curtis(f3, a, b, acc, eps, &err);
	printf("\nClenshaw Curtis: Integral of 1/sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	double f4(double x) {
		calls++;
		return log(x)/sqrt(x);
	}
	Q = adapt(f4, a, b, acc, eps, &err);
	expected = -4.0;
	printf("\nIntegral of ln(x)/sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));

	calls = 0;
	Q = clenshaw_curtis(f4, a, b, acc, eps, &err);
	printf("\nClenshaw Curtis: Integral of ln(x)/sqrt(x) from 0 to 1:\n");
	printf("Value = %lg\tEstimated error = %lg\tCalls = %i\n",Q,err,calls);
	printf("Expected = %lg\tActual error = %lg\n",expected,fabs(Q-expected));
	
	printf("\n\nHaving used the Clenshaw-Curtis variable transformation on both integrals with and without integrable divergencies at the ends of the intervals, we see a major improvement on the former and only a slightly slower convergence towards a solution on the latter.\n");

	calls = 0;
	acc=1e-15, eps=1e-15;
	printf("\n\nAbsolute accuracy goal = %g\nRelative accuracy goal = %g\n",acc,eps);
	printf("______________________________\n");
	double f5(double x) {
		calls++;
		return 4*sqrt(1-(1-x)*(1-x));
	}
	Q = adapt(f5, a, b, acc, eps, &err);
	expected = M_PI;
	printf("\nIntegral of 4*sqrt(1-(1-x)²) from 0 to 1:\n");
	printf("Value = \t%0.50lg\nExpected = \t%0.50lg\nEstimated error = \t%.50lg\nActual error = \t%.50lg\nCalls = \t%i\n",Q,expected,err,fabs(Q-expected),calls);
}
