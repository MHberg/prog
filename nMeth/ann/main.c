#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ann.h"
#include"ann2d.h"

int main() {
	int n = 10;
	double activationFunction(double x) {
		return x*exp(-x*x);
	}
	double functionToFit(double x) {
		return cos(5*x-1)*exp(-x*x);
	}
	ann* network = annAlloc(n, activationFunction);
	double a = -1, b = 1;
	int nx = 20;
	gsl_vector* xlist = gsl_vector_alloc(nx);
	gsl_vector* fxlist = gsl_vector_alloc(nx);
	for(int i=0; i<nx; i++) {
		double x = a+(b-a)*i/(nx-1);
		double fx = functionToFit(x);
		gsl_vector_set(xlist,i,x);
		gsl_vector_set(fxlist,i,fx);
	}
	for(int i=0; i<network->n; i++) {
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data,3*i+1,1);
		gsl_vector_set(network->data,3*i+2,1);
	}

	annTrain(network,xlist,fxlist);

	for(int i=0; i<xlist->size; i++) {
		double x = gsl_vector_get(xlist,i);
		double fx = gsl_vector_get(fxlist,i);
		printf("%g\t%g\n",x,fx);
	}
	printf("\n\n");

	double dz = 1.0/64;
	for(double z=a; z<=b; z+=dz) {
		double y = annFeedForward(network,z);
		printf("%g\t%g\n",z,y);
	}
	annFree(network);
	gsl_vector_free(xlist);
	gsl_vector_free(fxlist);
	printf("\n\n");

	//2D neural network
	n = 10;
	double activationFunction2d(double x, double y) {
		return x*exp(-x*x)+y*exp(-y*y);
	}
	double functionToFit2d(double x, double y) {
		return cos(5*x-1)*exp(-x*x)+cos(5*y-1)*exp(-y*y);
		//return cos(5*x-1)*exp(-x*x) + y*y;
		//return x*x-y*y;
	}
	ann2d* network2d = annAlloc2d(n, activationFunction2d);
	a = -1, b = 1;
       	double c = -1, d = 1;
	nx = 20;
	int ny = 20;
	xlist = gsl_vector_alloc(nx);
	gsl_vector* ylist = gsl_vector_alloc(ny);
	gsl_matrix* fxylist = gsl_matrix_alloc(nx,ny);
	for(int i=0; i<nx; i++) for(int j=0; j<ny; j++) {
		double x = a+(b-a)*i/(nx-1);
		double y = c+(d-c)*j/(ny-1);
		double fxy = functionToFit2d(x,y);
		gsl_vector_set(xlist,i,x);
		gsl_vector_set(ylist,j,y);
		gsl_matrix_set(fxylist,i,j,fxy);
	}
	for(int i=0; i<network2d->n; i++) {
		gsl_vector_set(network2d->data,5*i+0,a+(b-a)*i/(network2d->n-1));
		gsl_vector_set(network2d->data,5*i+1,1);
		gsl_vector_set(network2d->data,5*i+2,c+(d-c)*i/(network2d->n-1));
		gsl_vector_set(network2d->data,5*i+3,1);
		gsl_vector_set(network2d->data,5*i+4,1);
	}

	annTrain2d(network2d,xlist,ylist,fxylist);

	for(int i=0; i<xlist->size; i++) for(int j=0; j<ylist->size; j++) {
		double x = gsl_vector_get(xlist,i);
		double y = gsl_vector_get(ylist,j);
		double fxy = gsl_matrix_get(fxylist,i,j);
		printf("%g\t%g\t%g\n",x,y,fxy);
	}
	printf("\n\n");

	dz = 1.0/64;
	double dt = 1.0/64;
	for(double z=a; z<=b; z+=dz) {
		for(double t=c; t<=d; t+=dt) {
			double u = annFeedForward2d(network2d,z,t);
			printf("%g\t%g\t%g\n",z,t,u);
		}
		printf("\n");
	}
	annFree2d(network2d);
	gsl_vector_free(xlist);
	gsl_vector_free(ylist);
	gsl_matrix_free(fxylist);
}
