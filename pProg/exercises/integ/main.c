#include<gsl/gsl_integration.h>
#include<stdio.h>
#include<math.h>

double f (double x, void *params) {
	//double alpha = *(double *)params;
	double f = log(x)/sqrt(x);
	return f;
}

double norm ( double x, void *params) {
	double alpha = *(double *) params;
	double norm = 2*exp(-alpha*x*x);
	return norm;
}

double hamilton_int ( double x, void *params) {
	double alpha = *(double *) params;
	double hamilton_int = 2*(-alpha*alpha*x*x/2+alpha/2+x*x/2)*exp(-alpha*x*x);
	return hamilton_int;
}

int main() {
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);
	double result, error;
	double expected = -4.0;
	//double alpha = 1.0;
	gsl_function F;
	F.function = &f;
	F.params = NULL;
	gsl_integration_qags(&F, 0, 1, 0, 1e-7, 1000, w, &result, &error);
	printf ("result          = % .18f\n", result);
	printf ("exact result    = % .18f\n", expected);
	printf ("estimated error = % .18f\n", error);
	printf ("actual error    = % .18f\n", result - expected);
	printf ("intervals       = %zu\n", w->size);	

	printf ("\n\n#Harmonic oscillator energies\n#alpha \t\t  E(alpha)\n");
	
	for (int i = 1; i<300; i++) {

		double alpha = i/100.0;
		double result_norm, error_norm; 

		F.function = &norm;
		F.params = &alpha;
		gsl_integration_qagiu(&F, 0, 0, 1e-7, 1000, w, &result_norm, &error_norm);

		double result_hamilton, error_hamilton;
		F.function = &hamilton_int;

		gsl_integration_qagiu(&F, 0, 0, 1e-7, 1000, w, &result_hamilton,&error_hamilton);

		double E = result_hamilton/result_norm;

		printf("%f \t % .18f\n",alpha,E);
	}
	gsl_integration_workspace_free(w);
	return 0;
}
