#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include "eigen.h"
#define FMT "%7.3f"
#define MAX_PRINT 11

void printM(gsl_matrix* M) {
	for (int i = 0; i<M->size1; i++) {
		for (int j = 0; j<M->size2; j++) {
			double x = gsl_matrix_get(M,i,j);
			fprintf(stderr,FMT,x);
			fprintf(stderr,"\t");
		}
		fprintf(stderr,"\n");
	}
}

int main(int argc, char** argv) {

	int n = (argc>1? atoi(argv[1]):5);
	int N = (argc>2? atoi(argv[2]):1);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	for (int i=0; i<n; i++) for (int j=i; j<n; j++) {
		double x = rand() % 10;
		gsl_matrix_set(A,i,j,x);
		gsl_matrix_set(A,j,i,x);
	}
	gsl_matrix_memcpy(B,A);

	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);

	// The below code is for testing comparison using diagonalization from mainA (jacobi function instead of jacobi_eigbyeig
//	gsl_matrix* Vcp = gsl_matrix_alloc(n,n);
//	gsl_matrix_memcpy(Vcp,V);
//	gsl_vector* ecp = gsl_vector_alloc(n);
//	gsl_vector_memcpy(ecp,e);
//	gsl_matrix* Acp = gsl_matrix_alloc(n,n);
//	gsl_matrix* Bcp = gsl_matrix_alloc(n,n);
//	gsl_matrix_memcpy(Bcp,B);
//	gsl_matrix_memcpy(Acp,A);

//	int sweeps2 = jacobi(Acp,ecp,Vcp);
//	printf("%i\t%i\n",n,sweeps2);
//	if (n<MAX_PRINT) {
//		fprintf(stderr,"\nRandom symmetric matrix A: \n"); 
//		printM(Bcp);
//		fprintf(stderr,"\nResult of Jacobi diagonalization: \n");
//		fprintf(stderr,"\nEigenvalues: \n");
//		fprintf(stderr,"e = \n");
//		gsl_vector_fprintf(stderr,ecp,FMT);
//		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Bcp,Vcp,0.0,Acp);
//		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,Vcp,Acp,0.0,Bcp);
//		fprintf(stderr,"\n(V^T)AV (should be diagonal with values of e) = \n");
//		printM(Bcp);
//	}
//	gsl_matrix_free(Acp); gsl_matrix_free(Vcp); gsl_matrix_free(Bcp); gsl_vector_free(ecp);

	int sweeps = jacobi_eigbyeig(A,e,V,N);
	printf("%i\t%i\n",n,sweeps);
	if (n<MAX_PRINT) {
		fprintf(stderr,"Jacobi diagonalization eigenvalue-by-eigenvalue: \nMatrix size %i\nNumber of eigenvalues to be calculated: %i\n",n,N);
		fprintf(stderr,"\nRandom symmetric matrix A: \n"); 
		printM(B);
		fprintf(stderr,"\nResult of Jacobi diagonalization: \n");
		if (N>1) fprintf(stderr,"\n%i lowest eigenvalues: \n",N);
		else fprintf(stderr,"Lowest eigenvalue: \n");

		for (int i = 0; i<N; i++) {
			fprintf(stderr,FMT,gsl_vector_get(e,i));
			fprintf(stderr,"\n");
		}
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B,V,0.0,A);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,B);
		fprintf(stderr,"\n(V^T)AV (should be diagonal with eigenvalues on diagonal) = \n");
		printM(B);
	}
	gsl_matrix_free(A); gsl_matrix_free(V); gsl_matrix_free(B); gsl_vector_free(e);

	if (n<MAX_PRINT) {
		n = 3;
		A = gsl_matrix_calloc(n,n);
		B = gsl_matrix_calloc(n,n);
		gsl_matrix_set(A,0,0,24);
		gsl_matrix_set(A,1,1,-28);
		gsl_matrix_set(A,2,2,3);
		
		gsl_matrix_memcpy(B,A);

		e = gsl_vector_alloc(n);
		V = gsl_matrix_alloc(n,n);

		fprintf(stderr,"\nArgue, that the corresponding diagonal element (the diagonal element in the first row) is the lowest eigenvalue:\n");
		fprintf(stderr,"# By picking out the eigenvalues 1 by 1 we get the same order as with the diagonalization of the whole matrix and hence by applying both algorithms to the same matrix we can compare the eigenvalues and see that the diagonal element we get from eliminating the off-diagonal elements in the first row only is indeed the lowest eigenvalue. Another example is giving the algorithm a diagonal matrix with an unsorted arrangement of eigenvalues:\n");

		fprintf(stderr,"\nDiagonal matrix A with known eigenvalues unsorted: \n"); 
		printM(B);

		fprintf(stderr,"\nAfter running eig-by-eig Jacobi diagonalization to get the first eigenvalue we get:\n");
		jacobi_eigbyeig(A,e,V,1);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B,V,0.0,A);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,B);
		fprintf(stderr,"\n(V^T)AV (should be diagonal with lowest eigenvalue on first diagonal) = \n");
		printM(B);

		fprintf(stderr,"\nArgue, that the eliminated elements in the first row will not be affected and can therefore be omitted from the elimination loop:\n");
		fprintf(stderr,"# Since q>p and we only change elements from A with the following indices: (p,i), (q,i), (p,p), (q,q), (p,q) (only considering the upper triangle of A since A is symmetrical), when p is advanced we no longer consider elements of row p-1 in the sweep and hence these elements are not touched.\n\n");

		fprintf(stderr,"Argue that the corresponding diagonal element (the diagonal element in the second row) is the second lowest eigenvalue:\n");
		fprintf(stderr,"# By the same argument as for the lowest eigenvalue we can see that the corresponding diagonal element from removing off-diagonal elements of the second row is indeed the second lowest eigenvalue. Again using the unsorted diagonal matrix A from just above and running eig-by-eig diagonalization on it to find the two first eigenvalues we get:\n");

		gsl_matrix_free(A); gsl_matrix_free(V); gsl_matrix_free(B); gsl_vector_free(e);

		A = gsl_matrix_calloc(n,n);
		B = gsl_matrix_calloc(n,n);
		gsl_matrix_set(A,0,0,24);
		gsl_matrix_set(A,1,1,-28);
		gsl_matrix_set(A,2,2,3);
		
		gsl_matrix_memcpy(B,A);

		e = gsl_vector_alloc(n);
		V = gsl_matrix_alloc(n,n);
		jacobi_eigbyeig(A,e,V,2);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B,V,0.0,A);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,B);
		fprintf(stderr,"\n(V^T)AV (should be diagonal with lowest two eigenvalues on first two diagonals) = \n");
		printM(B);

		fprintf(stderr,"\nFind out how to change the formulas for the rotation angle to obtain the largest eigenvalues instead of the lowest:\n");
		fprintf(stderr,"# By changing the rotation angle phi to phi+pi/2 we get the largest eigenvalues first instead of the lowest.\n\n");
	}
}
