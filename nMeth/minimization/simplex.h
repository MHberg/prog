int simplex_initiate(
	double (*fun)(double*),
	double** simplex, double* fun_values, int d,
	int* hi, int* lo, double* centroid);

void simplex_update(
		double** simplex, double* fun_values, int d,
		int* hi, int* lo, double* centroid);

void reflection(
	double* highest, double* centroid, int d,
	double* reflected);

void expansion(
	double* highest, double* centroid, int d,
	double* expanded);

void contraction(
	double* highest, double* centroid, int d,
	double* contracted);

void reduction(
	double** simplex, int d,
	int lo);

double size(double** simplex,int d);

int downhill_simplex(
        double(*fun)(double*),
        double** simplex, int d,
        double simplex_size_goal);
