#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	komplex_print("a=",a);
	komplex_print("b=",b);

	printf("\ntesting komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);

/* the following (except subtraction) is optional */

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");
	
//subtraction
	printf("\ntesting komplex_sub...\n");
	komplex R2 = {-2,-2};
	komplex r2 = komplex_sub(a,b);
	komplex_print("a-b should = ",R2);
	komplex_print("a-b actually = ",r2);
	
	if( komplex_equal(R2,r2,TINY,TINY) )
		printf("test 'sub' passed :) \n");
	else
		printf("test 'sub' failed: debug me, please... \n\n");

//multiplication
	printf("\ntesting komplex_mul...\n");
	komplex R3 = {-5,10};
	komplex r3 = komplex_mul(a,b);
	komplex_print("a*b should = ",R3);
	komplex_print("a*b actually = ",r3);

	if( komplex_equal(R3,r3,TINY,TINY) )
		printf("test 'mul' passed :) \n");
	else
		printf("test 'mul' failed: debug me, please... \n\n");
}
