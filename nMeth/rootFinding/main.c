#include<stdio.h>
#include"rootFinding.h"

int main() {
	printf("Root finding (Newton, numerical Jacobian):\n\n");
	rootFindNumJac();
	fprintf(stderr,"\n\n");

	printf("Root finding (Newton, analytical Jacobian):\n\n");
	rootFindAnJac();
	fprintf(stderr,"\n\n");

	printf("Root finding (GSL multiroot):\n\n");
	rootFindGSL();
}
