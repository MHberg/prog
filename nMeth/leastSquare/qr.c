#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_blas.h>

void backsub(gsl_matrix* R, gsl_vector* x) {
	int m=R->size1;
	for(int i=m-1;i>=0;i--){
		double s=0;
		for(int k=i+1;k<m;k++)
			s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
		gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
	}

}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {

	int c = A->size2;

	for (int i = 0; i<c; i++) {
		gsl_vector_view qi = gsl_matrix_column(A,i);

		double Rii = gsl_blas_dnrm2(&qi.vector);
		gsl_matrix_set(R,i,i,Rii);
		gsl_vector_scale(&qi.vector,1/Rii);

		for (int j = i+1; j<c; j++) {
			gsl_vector_view qj = gsl_matrix_column(A,j);
			double Rij = 0;
			gsl_blas_ddot(&qi.vector,&qj.vector,&Rij);
			gsl_matrix_set(R,i,j,Rij);
			gsl_blas_daxpy(-Rij,&qi.vector,&qj.vector);
		}
	}
}


void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x) {
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	backsub(R,x);
}

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B) {
	int n = R->size1;
	gsl_matrix_set_identity(B);
	gsl_vector* x = gsl_vector_calloc(R->size1);
	for (int i = 0; i<n; i++) {
		gsl_vector_view b = gsl_matrix_column(B,i);
		qr_gs_solve(Q, R, &b.vector, x);
		gsl_matrix_set_col(B,i,x);
	}
}
