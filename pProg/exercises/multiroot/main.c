#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>

struct rparams {
	double a;
	double b;
	double c;
};

double func (double x, double y) {
	return (1-x)*(1-x)+100*(y-(x*x))*(y-(x*x));
}

int rosenbrock_f (const gsl_vector* x, void *params, gsl_vector * f)
{
	double a = ((struct rparams *) params) -> a;
	double b = ((struct rparams *) params) -> b;
	double c = ((struct rparams *) params) -> c;

	const double x0 = gsl_vector_get(x,0);
	const double x1 = gsl_vector_get(x,1);

	const double y0 = a*(x0-1)+b*((x0*x0)-x1);
	const double y1 = c*(x1-(x0*x0));

	gsl_vector_set(f,0,y0);
	gsl_vector_set(f,1,y1);
	
	return GSL_SUCCESS;
}

void print_state (size_t iter, gsl_multiroot_fsolver * s)
{
  printf ("iter = %3lu x = % .3f % .3f "
          "f(x) = % .3e % .3e\n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->f, 0),
          gsl_vector_get (s->f, 1));
}

int main() {
	FILE *file = fopen("data.txt", "w");
	if (file == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	fprintf(file,"#x\ty\tf(x,y)\n");
	for (int i = -200; i<200; i++) {
		for (int j = -100; j<300; j++) {
			double x = i/100.0;
			double y = j/100.0;
			fprintf(file,"%lg\t",x);
			fprintf(file,"%lg\t",y);
			fprintf(file,"%lg\n",func(x,y));
		}
		fprintf(file,"\n");
	}
	fclose(file);

	FILE *solutionPathFile = fopen("solutionPath.txt","w");
	if (solutionPathFile == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	fprintf(solutionPathFile,"#x\ty\n");

	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;

	int status;
	size_t iter = 0;

	const size_t n = 2;
	struct rparams p = {2.0, 400.0, 200.0};
	gsl_multiroot_function f = {&rosenbrock_f, n, &p};

	double x_init[2] = {-10.0, 5.0};
	gsl_vector *x = gsl_vector_alloc(n);

	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);

	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc(T,2);
	gsl_multiroot_fsolver_set(s, &f, x);

	print_state(iter, s);

	do
	{
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);

		print_state(iter, s);
		fprintf(solutionPathFile,"%lg\t%lg\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));

		if (status) break;

		status = gsl_multiroot_test_residual(s->f, 1e-7);
	}
	while (status == GSL_CONTINUE && iter < 1000);

	printf("status = %s\n", gsl_strerror(status));

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	return 0;
}
