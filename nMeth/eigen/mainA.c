#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include "eigen.h"
#define FMT "%7.3f"
#define MAX_PRINT 11

void printM(gsl_matrix* M) {
	for (int i = 0; i<M->size1; i++) {
		for (int j = 0; j<M->size2; j++) {
			double x = gsl_matrix_get(M,i,j);
			fprintf(stderr,FMT,x);
			fprintf(stderr,"\t");
		}
		fprintf(stderr,"\n");
	}
}

int main(int argc, char** argv) {

	int n = (argc>1? atoi(argv[1]):5);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	for (int i=0; i<n; i++) for (int j=i; j<n; j++) {
		double x = rand() % 10;
		gsl_matrix_set(A,i,j,x);
		gsl_matrix_set(A,j,i,x);
	}
	gsl_matrix_memcpy(B,A);

	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	int sweeps = jacobi(A,e,V);
	printf("%i\t%i\n",n,sweeps);
	if (n<MAX_PRINT) {
		fprintf(stderr,"\nFull matrix cyclic Jacobi diagonalization: \n"); 
		fprintf(stderr,"\nRandom symmetric matrix A: \n"); 
		printM(B);
		fprintf(stderr,"\nResult of Jacobi diagonalization: \n");
		fprintf(stderr,"\nEigenvalues: \n");
		fprintf(stderr,"e = \n");
		gsl_vector_fprintf(stderr,e,FMT);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B,V,0.0,A);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,B);
		fprintf(stderr,"\n(V^T)AV (should be diagonal with values of e) = \n");
		printM(B);
	}	
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(B);
	gsl_vector_free(e);

}
