#include<stdio.h>
#include<math.h>
#include"integrator.h"
#include<gsl/gsl_integration.h>
#include<omp.h>

void mainAC();
void mainB();

int main()
{
	#pragma omp parallel sections
	// Part A+C and part B are split up into two separate threads and run in parallel using OpenMP
	{
		#pragma omp section //first thread running part A and C
		{
			mainAC();
		}
		#pragma omp section //second thread running part B
		{
			mainB();
		}
	}
	printf("\n_____________________________OpenMP__________________________________\n");
	printf("\nComparing the time it takes to run the program with OpenMP to the time it takes without we see a slight drop in real time when using OpenMP e.g. \nTime without:	real	0m0.450s\nTime with:	real	0m0.322s\n\nAs can be seen in the above the output of the three parts of the exercise are now mixed since each of the two threads prints in parallel. I have separated output into two files called outAC.txt and outB.txt with outAC.txt also containing this message.\n\n");
}
