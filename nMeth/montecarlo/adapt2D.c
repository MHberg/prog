#include<stdio.h>
#include<math.h>
#include"integrator.h"

double adapt2D(double f(double* x), double a, double b, double c(double x), double d(double x), double acc, double eps, double* err) {
	double F(double x) {
		double f2(double y) {
			double p[] = {x,y};
			return f(p);
		}
		return adapt(f2,c(x),d(x),acc,eps,err);
	}
	return adapt(F, a, b, acc, eps, err);
}
