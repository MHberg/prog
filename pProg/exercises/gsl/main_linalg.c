#include<stdio.h>
#include "gsl/gsl_linalg.h"
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_cblas.h"
#include<math.h>

int main() {
	gsl_matrix* A = gsl_matrix_alloc(3,3);
	gsl_vector* x = gsl_vector_alloc(3);
	gsl_vector* b = gsl_vector_alloc(3);
	gsl_matrix* A_copy = gsl_matrix_alloc(3,3);
	
	gsl_matrix_set(A,0,0,6.13);
	gsl_matrix_set(A,1,0,8.08);
	gsl_matrix_set(A,2,0,-4.36);
	gsl_matrix_set(A,0,1,-2.90);
	gsl_matrix_set(A,1,1,-6.31);
	gsl_matrix_set(A,2,1,1.00);
	gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,2,0.19);
	
	gsl_matrix_memcpy(A_copy, A);

	double b0 = 6.23;
	double b1 = 5.37;
	double b2 = 2.29;
	gsl_vector_set(b,0,b0);
	gsl_vector_set(b,1,b1);
	gsl_vector_set(b,2,b2);

	gsl_linalg_HH_solve(A,b,x);
	
	printf("x0 = %lg\n",gsl_vector_get(x,0));
	printf("x1 = %lg\n",gsl_vector_get(x,1));
	printf("x2 = %lg\n",gsl_vector_get(x,2));

	gsl_vector* y = gsl_vector_alloc(3);
	gsl_blas_dgemv(CblasNoTrans,1,A_copy,x,0,y);
	
	printf("\nA*x = \n");	
	printf("y0 = %lg\n",gsl_vector_get(y,0));
	printf("y1 = %lg\n",gsl_vector_get(y,1));
	printf("y2 = %lg\n",gsl_vector_get(y,2));

	double eps = 1e-6;
	if(fabs(gsl_vector_get(y,0)-b0)<=eps && fabs(gsl_vector_get(y,1)-b1) <= eps && fabs(gsl_vector_get(y,2)-b2)<=eps) {
		printf("\nComputation was successful\n");
	}

	gsl_matrix_free(A);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(y);

	return 0;
}
