#include<assert.h>
#include<stdlib.h>
#include<math.h>
#include"interpolFunctions.h"

qspline* qspline_alloc(int n, double *x, double *y) { //builds qspline
	qspline *s = (qspline*) malloc(sizeof(qspline)); //spline
	s->b = (double*) malloc((n-1)*sizeof(double)); // b_i
	s->c = (double*) malloc((n-1)*sizeof(double)); //c_i
	s->x = (double*) malloc(n*sizeof(double)); //x_i
	s->y = (double*) malloc(n*sizeof(double)); //y_i
	s->n = n; for (int i=0; i<n; i++) {s->x[i]=x[i]; s->y[i]=y[i];}
	int i; double p[n-1], h[n-1]; //VLA from C99
	for (i=0; i<n-1; i++) {h[i] = x[i+1]-x[i]; p[i] = (y[i+1]-y[i])/h[i];}
	s->c[0] = 0; //bottom up recursion:
	for (i=0; i<n-2; i++) s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
	s->c[n-2]/=2; //top down recursion:
	for (i=n-3; i>=0; i--) s->c[i] = (p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
	for (i=0; i<n-1; i++) {
		s->b[i] = p[i]-s->c[i]*h[i];
		//fprintf(stderr,"i = %i, b = %lg\tc = %lg\n",i+1,s->b[i],s->c[i]);
	}
	return s;
}

double qspline_eval(qspline *s, double z) { //evaluates s(z)
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1; //binary search:
	while (j-i>1) {int m=(i+j)/2; 
		if(z>s->x[m]) i=m; 
		else j=m;
	}
	double h = z-s->x[i];
	return s->y[i]+h*(s->b[i]+h*s->c[i]);
}

void qspline_free(qspline *s) { //free allocated memory
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}

double qspline_derivative(qspline *s, double z) {
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1; //binary search:
	while (j-i>1) {int m=(i+j)/2; 
		if(z>s->x[m]) i=m; 
		else j=m;
	}
	return s->b[i]+2*s->c[i]*(z-s->x[i]);
}

double qspline_integral(qspline *s, double z) {
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1; //binary search:
	double result = 0;
	while (j-i>1) {int m=(i+j)/2; 
		if(z>s->x[m]) i=m; 
		else j=m;
	}
	for (int k = 0; k<=i; k++) {
		double yi = s->y[k];
		double xi = s->x[k];
		double bi = s->b[k];
		double ci = s->c[k];
		double bot = xi;
		double top;
		if (s->x[k+1] > z) {
			top = z;
		}
		else if (s->x[k+1] <= z) {
			top = s->x[k+1];
		}
		result += yi*top+1.0/2*bi*top*top-bi*xi*top+1.0/3*ci*top*top*top+ci*xi*xi*top-ci*top*top*xi
				-(yi*bot+1.0/2*bi*bot*bot-bi*xi*bot+1.0/3*ci*bot*bot*bot+ci*xi*xi*bot-ci*bot*bot*xi);
	}
	return result;
}

double linterp_integ(int n, double *x, double *y, double z) {
	assert(n>1 && z >= x[0] && z <= x[n-1]);
	int i = 0, j = n-1;
	double result = 0;
	while (j-i>1) {
		int m = (i+j)/2;
		if (z>x[m]) {
			i = m;
		} else {
			j = m;
		}
	}
	for (int k = 0; k<=i; k++) {
		double p_k = (y[k+1]-y[k])/(x[k+1]-x[k]);
		if (x[k+1] > z) {
			result += p_k*(1.0/2*z-x[k])*z+y[k]*z-(p_k*(1.0/2*x[k]-x[k])*x[k]+y[k]*x[k]);
		}
		else if (x[k+1] <= z) {
			result += p_k*(1.0/2*x[k+1]-x[k])*x[k+1]+y[k]*x[k+1]-(p_k*(1.0/2*x[k]-x[k])*x[k]+y[k]*x[k]);
		}
	}
	return result;
}

double linterp(int n, double *x, double *y, double z) {
	//fprintf(stderr,"x[n] = %lg, x[n-1] = %lg, z = %lg\n",x[n],x[n-1],z);
	assert(n>1 && z >= x[0] && z <= x[n-1]);
	int i=0, j = n-1; 
	while (j-i>1) {
		int m = (i+j)/2;
		if (z>x[m]) {
			i = m;
		} else {
			j = m;
		}
	}
	return 	y[i] + (y[i+1] - y[i]) / (x[i+1] - x[i]) * (z - x[i]);
}	


#define f(x) sin(x)


