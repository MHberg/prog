#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"ode.h"

double f1(double x) {
	return sin(x);
}

double f2(double x) {
	return sin(x)*sin(x);
}

double f3(double x) {
	return x*x;
}

void F1(double x, gsl_vector* y, gsl_vector* dydx) {
	gsl_vector_set(dydx,0,f1(x));
}

void F2(double x, gsl_vector* y, gsl_vector* dydx) {
	gsl_vector_set(dydx,0,f2(x));
}

void F3(double x, gsl_vector* y, gsl_vector* dydx) {
	gsl_vector_set(dydx,0,f3(x));
}

int main() {
	// f(x) = sin(x)
	int n = 1;
	int max = 100000;
	gsl_vector* xlist = gsl_vector_calloc(max);
	gsl_matrix* ylist = gsl_matrix_calloc(max,n);
	double a = 0, b = 2*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
	gsl_vector_set(xlist,0,a);
	gsl_matrix_set(ylist,0,0,0);
	int k = ode_driver(xlist,ylist,b,h,acc,eps,max,F1);
	printf("# Numerical solution to definite integral as differential equation (rkstep12):\n");
	printf("# x\tf(x)\t-cos(x) (exact solution):\n");
	for(int i=0; i<k; i++) {
		printf("%0.5g\t%0.5g\t%0.5g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0),-cos(gsl_vector_get(xlist,i))+1);
	}
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);
	printf("\n\n");

	// f(x) = sin(x)^2
	xlist = gsl_vector_calloc(max);
	ylist = gsl_matrix_calloc(max,n);
	a = 0, b = 2*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
	gsl_vector_set(xlist,0,a);
	gsl_matrix_set(ylist,0,0,0);
	k = ode_driver(xlist,ylist,b,h,acc,eps,max,F2);
	printf("# Numerical solution to definite integral as differential equation (rkstep12):\n");
	printf("# x\tf(x)\t1/2(x-sin(x)cos(x)) (exact solution):\n");
	for(int i=0; i<k; i++) {
		printf("%0.5g\t%0.5g\t%0.5g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0),1/2.0*(gsl_vector_get(xlist,i)-sin(gsl_vector_get(xlist,i))*cos(gsl_vector_get(xlist,i))));
	}
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);
	printf("\n\n");
	
	// f(x) = x^2
	xlist = gsl_vector_calloc(max);
	ylist = gsl_matrix_calloc(max,n);
	a = 0, b = 2*M_PI, h = 0.1, acc = 0.01, eps = 0.01;
	gsl_vector_set(xlist,0,a);
	gsl_matrix_set(ylist,0,0,0);
	k = ode_driver(xlist,ylist,b,h,acc,eps,max,F3);
	printf("# Numerical solution to definite integral as differential equation (rkstep12):\n");
	printf("# x\tf(x)\t1/3x^3 (exact solution):\n");
	for(int i=0; i<k; i++) {
		printf("%0.5g\t%0.5g\t%0.5g\n",gsl_vector_get(xlist,i),gsl_matrix_get(ylist,i,0),gsl_vector_get(xlist,i)*gsl_vector_get(xlist,i)*gsl_vector_get(xlist,i)/3.0);
	}
	gsl_vector_free(xlist);
	gsl_matrix_free(ylist);
}
