void lsfit(gsl_vector *x, gsl_vector *y, gsl_vector *dy, int m, double (*fitfuns)(int i, double x), gsl_vector* c, gsl_matrix* S);
