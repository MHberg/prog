#include <math.h>

void reflection( double* highest, double* centroid, int dim, double* reflected) {
	for (int i=0; i<dim; i++) reflected[i] = 2*centroid[i]-highest[i];
}

void expansion( double* highest, double* centroid, int dim, double* expanded) {
	for (int i=0; i<dim; i++) expanded[i] = 3*centroid[i]-2*highest[i];
}

void contraction( double* highest, double* centroid, int dim, double* contracted) {
	for (int i=0; i<dim; i++) contracted[i] = 0.5*centroid[i]+0.5*highest[i];
}

void reduction( double** simplex, int dim, int lo) {
	for (int k=0; k<dim+1; k++) if (k!=lo)
		for (int i=0; i<dim; i++)
			simplex[k][i] = 0.5*(simplex[k][i]+simplex[lo][i]);
}

double distance(double* a, double* b, int dim) {
	double s = 0;
	for (int i=0; i<dim; i++) s += pow(a[i]-b[i],2);
	return sqrt(s);
}

double size(double** simplex,int dim) {
	double s = 0;
	for (int k=1; k<dim+1; k++) {
		double dist=distance(simplex[0],simplex[k],dim);
		if (dist>s) s = dist;
	}
	return s;
}
