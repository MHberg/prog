void rkstep12(
	double x,                                  /* the current value of the variable */
	double h,                                  /* the step to be taken */
	gsl_vector* yx,                                /* the current value y(x) of the sought function */
	void f(double x, gsl_vector* y, gsl_vector* dydx), /* the right-hand-side, dydt = f(x,y) */
	gsl_vector* yxh,                               /* output: y(x+h) */
	gsl_vector* err                                /* output: error estimate dy */
);

int ode_driver(
	gsl_vector* xlist,
	gsl_matrix* ylist,
	double b,
	double h,
	double acc,
	double eps,
	int max,
	void (*f)(double x, gsl_vector* y, gsl_vector* dydx));
